#### INSTALL
```bash
git clone https://gitlab.com/fonte-gaia/fonte-gaia-bib-c.git
cd fonte-gaia-bib-c
cp .env.dist .env
cp db.ini.dist db.ini
cp .htaccess_dist .htaccess
sudo apt-get install docker.io #si docker n'est pas installé
docker-compose up -d # ou bien sudo docker-compose up -d
docker-compose exec omeka bash # ou bien sudo docker-compose exec omeka bash
  > chmod -R 777 /var/www/html/files /var/www/html/plugins /var/www/html/themes; exit
# Personnaliser le cas échéant config.ini et relancer le docker
sudo docker-compose stop
sudo docker-compose up -d --build

# Installation des plugins et thèmes
git submodule update --init --recursive -f #si erreur, voir plus bas "Résolution de bug"
```
* Go to http://localhost:8901/install/install.php

#### VARIABLES DE `.ENV`  

* **Si deux versions de l'image docker sont déployées sur un même serveur, veillez à leur donner un `COMPOSE_PROJECT_NAME` différent** (ex: fg-prod et fg-dev) pour éviter tout conflit lors des commandes docker-compose (risque de réécriture de l'image en production lors des commandes sur l'image de développement)
* **Veillez à choisir des ports différents pour une seconde image en parallèle**  

```bash
COMPOSE_PROJECT_NAME=fg-prod      # ex fg-prod / fg-dev
#MYSQL CONF  
MYSQL_ROOT_PASSWORD=omeka          
MYSQL_DATABASE=omeka
MYSQL_USER=omeka
MYSQL_PASSWORD=omeka
# PORTS
OMEKA_PORT=8901                  # ex 8902 en dev
SOLR_PORT=8983                   # ex 8984 en dev
```


#### THEME & PLUGINS
* Aller sur https://omeka.org/classic/plugins/ et copier l'url de téléchargement direct du plugin ($url)
* Aller sur le serveur (en ssh), dans le dossier du projet
* Aller dans le sous-dossier dédié aux extensions (cd plugins/) et récupérer le zip du plugin : curl -LO $url
* Dé-zipper et supprimer le zip : unzip $zipfile ; rm $zipfile
* Tout le reste se déroule directement sur le site lui-même (activation et paramétrage du plugin)
* Rem. Idem pour un thème sauf que le sous-dossier dédié est themes/

##### SolRSearch Plugin

* Un serveur Solr dockerisé est désormais ajouté à l'image (port part défaut de Solr : `8389`)
* La configuration du Core Solr destiné à Omeka est montée comme un volume depuis le dossier `/plugins/SolrSearch/solr-core/` qui contient lui même un dossier `/omeka`
* Pour connecter SolR à Omeka :
  * installer le plugin `SolrSearch` dans l'interface admin d'Omeka.
  * Puis configurer le plugin à l'aide du menu lattéral `Recherche Solr`. Dans le 1er onglet `Configuration du serveur` :
    * Nom d'hôte du serveur = `solr` (géré grâce à l'élément `links` dans `docker-compose.yml`)
    * Port du serveur = `8983`
    * URL du coeur = `/solr/omeka/`

### NOTES
* config.ini a été configuré pour utiliser le smtp de notre hébergeur

### Résolution de bug
#### Commande `git submodule update --init --recursive -f`
* Si un module pose problème avec un message du genre :
```bash
fatal: erreur distante : upload-pack: not our ref abe91a6a9e4af12b05d84cac608fed4190ce2e87
fatal: The remote end hung up unexpectedly
Chemin de sous-module 'plugins/CsvImport' récupéré, mais il ne contenait pas abe91a6a9e4af12b05d84cac608fed4190ce2e87. La récupération directe de ce commit a échoué.
```
alors suivre la procédure suivante (en remplacant {submodule path} par le chemin du module concerné ; plugins/CsvImport dans l'exemple ci-dessus) :
```bash
cd {submodule path}
git reset --hard origin/master
cd -
git clean -n
git add {submodule path}
git commit -m "Clean module {submodule path}"
git submodule update --init --recursive
```
