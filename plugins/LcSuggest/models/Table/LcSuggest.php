<?php
/**
 * Library of Congress Suggest
 *
 * @copyright Copyright 2007-2012 Roy Rosenzweig Center for History and New Media
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GPLv3
 */

/**
 * The lc_suggests table.
 *
 * @package Omeka\Plugins\LcSuggest
 */
class Table_LcSuggest extends Omeka_Db_Table
{
    /**
     * List of suggest endpoints made available by the Library of Congress
     * Authorities and Vocabularies service.
     *
     * The keys are URLs to the authority/vocabulary suggest endpoints. The
     * values are arrays containing the authority/vocabulary name and the URL to
     * the authority/vocabulary description page.
     *
     * These authorities and vocabularies have been selected due to their large
     * size and suitability to the autosuggest feature. Vocabularies not
     * explicitly included here may be redundant or better suited as a full list
     * controlled vocabulary.
     *
     * @see http://id.loc.gov/
     */
    private $_suggestEndpoints = array(
      'NuovoSoggetario'  => array(
            'name' => 'Tout Nuovo Soggetario',
            'url'  => 'NUOVO',
        ),
      'https://data.bnf.fr/sparql?query=PREFIX%20dcterms:%20%3Chttp://purl.org/dc/terms/%3E%0APREFIX%20skos:%20%3Chttp://www.w3.org/2004/02/skos/core%23%3E%0ASELECT%20DISTINCT%20?label%0AWHERE%20%7B%0A?resource%20skos:inScheme%20%3Chttp://data.bnf.fr/vocabulary/rameau%3E;%0Askos:prefLabel%20?label%20.%0A%20%20filter%20contains(?label,%20%22XXXX%22)%20%20%0A%7D%0AORDER%20BY%20?label%0ALIMIT%2025&format=application/json'  => array(
            'name' => 'Tout RAMEAU',
            'url'  => 'https://data.bnf.fr/fr/vocabulary/rameau',
        ),
        'https://data.bnf.fr/sparql?query=PREFIX%20dcterms:%20%3Chttp://purl.org/dc/terms/%3E%0APREFIX%20skos:%20%3Chttp://www.w3.org/2004/02/skos/core%23%3E%0ASELECT%20DISTINCT%20?label%0AWHERE%20%7B%0A?sujet%20dcterms:isPartOf%20%3Chttp://data.bnf.fr/vocabulary/rameau/r160%3E%20;%0Askos:prefLabel%20?label.%0A%7D&format=application/json' => array(
            'name' => 'RAMEAU - Personnages fictifs, mythologiques ou légendaires, divinités',
            'url'  => 'https://data.bnf.fr/fr/vocabulary/rameau/r160',
        ),
        'https://data.bnf.fr/sparql?query=PREFIX%20dcterms:%20%3Chttp://purl.org/dc/terms/%3E%0APREFIX%20skos:%20%3Chttp://www.w3.org/2004/02/skos/core%23%3E%0ASELECT%20DISTINCT%20?label%0AWHERE%20%7B%0A?sujet%20dcterms:isPartOf%20%3Chttp://data.bnf.fr/vocabulary/rameau/r161%3E%20;%0Askos:prefLabel%20?label.%0A%7D&format=application/json' => array(
            'name' => 'RAMEAU - Nom de collectivité',
            'url'  => 'https://data.bnf.fr/fr/vocabulary/rameau/r161',
        ),
        'https://data.bnf.fr/sparql?query=PREFIX%20dcterms:%20%3Chttp://purl.org/dc/terms/%3E%0APREFIX%20skos:%20%3Chttp://www.w3.org/2004/02/skos/core%23%3E%0ASELECT%20DISTINCT%20?label%0AWHERE%20%7B%0A?sujet%20dcterms:isPartOf%20%3Chttp://data.bnf.fr/vocabulary/rameau/r163%3E%20;%0Askos:prefLabel%20?label.%0A%7D&format=application/json' => array(
            'name' => 'RAMEAU - Titre propre d’anonyme',
            'url'  => 'https://data.bnf.fr/fr/vocabulary/rameau/r163',
        ),
        'https://data.bnf.fr/sparql?query=PREFIX%20dcterms:%20%3Chttp://purl.org/dc/terms/%3E%0APREFIX%20skos:%20%3Chttp://www.w3.org/2004/02/skos/core%23%3E%0ASELECT%20DISTINCT%20?label%0AWHERE%20%7B%0A?sujet%20dcterms:isPartOf%20%3Chttp://data.bnf.fr/vocabulary/rameau/r164%3E%20;%0Askos:prefLabel%20?label.%0A%7D&format=application/json' => array(
            'name' => 'RAMEAU - Titres de publications en série (périodiques et collections de monographies)',
            'url'  => 'https://data.bnf.fr/fr/vocabulary/rameau/r164',
        ),
        'https://data.bnf.fr/sparql?query=PREFIX%20dcterms:%20%3Chttp://purl.org/dc/terms/%3E%0APREFIX%20skos:%20%3Chttp://www.w3.org/2004/02/skos/core%23%3E%0ASELECT%20DISTINCT%20?label%0AWHERE%20%7B%0A?sujet%20dcterms:isPartOf%20%3Chttp://data.bnf.fr/vocabulary/rameau/r165%3E%20;%0Askos:prefLabel%20?label.%0A%7D&format=application/json' => array(
            'name' => 'RAMEAU - Titre uniforme textuel',
            'url'  => 'https://data.bnf.fr/fr/vocabulary/rameau/r165',
        ),
        'https://data.bnf.fr/sparql?query=PREFIX%20dcterms:%20%3Chttp://purl.org/dc/terms/%3E%0APREFIX%20skos:%20%3Chttp://www.w3.org/2004/02/skos/core%23%3E%0ASELECT%20DISTINCT%20?label%0AWHERE%20%7B%0A?sujet%20dcterms:isPartOf%20%3Chttp://data.bnf.fr/vocabulary/rameau/r166%3E%20;%0Askos:prefLabel%20?label.%0A%7D&format=application/json' => array(
            'name' => 'RAMEAU - Noms communs (et noms de peuples, de batailles, etc.)',
            'url'  => 'https://data.bnf.fr/fr/vocabulary/rameau/r166',
        ),
        'https://data.bnf.fr/sparql?query=PREFIX%20dcterms:%20%3Chttp://purl.org/dc/terms/%3E%0APREFIX%20skos:%20%3Chttp://www.w3.org/2004/02/skos/core%23%3E%0ASELECT%20DISTINCT%20?label%0AWHERE%20%7B%0A?sujet%20dcterms:isPartOf%20%3Chttp://data.bnf.fr/vocabulary/rameau/r167%3E%20;%0Askos:prefLabel%20?label.%0A%7D&format=application/json' => array(
            'name' => 'RAMEAU - Noms géographiques',
            'url'  => 'https://data.bnf.fr/fr/vocabulary/rameau/r167',
        ),
        'https://data.bnf.fr/sparql?query=PREFIX%20dcterms:%20%3Chttp://purl.org/dc/terms/%3E%0APREFIX%20skos:%20%3Chttp://www.w3.org/2004/02/skos/core%23%3E%0ASELECT%20DISTINCT%20?label%0AWHERE%20%7B%0A?sujet%20dcterms:isPartOf%20%3Chttp://data.bnf.fr/vocabulary/rameau/r168%3E%20;%0Askos:prefLabel%20?label.%0A%7D&format=application/json' => array(
            'name' => 'RAMEAU - Subdivisions chronologiques',
            'url'  => 'https://data.bnf.fr/fr/vocabulary/rameau/r168',
        ),
    );

    /**
     * Find a suggest record by element ID.
     *
     * @param int|string $elementId
     * @return LcSuggest|null
     */
    public function findByElementId($elementId)
    {
        $select = $this->getSelect()->where('element_id = ?', $elementId);
        return $this->fetchObject($select);
    }

    /**
     * Get the suggest endpoints.
     *
     * @return array
     */
    public function getSuggestEndpoints()
    {
        return $this->_suggestEndpoints;
    }
}
