<?php
$formStem = $block->getFormStem();
$options = $block->getOptions();
?>
<div class="item-details">
    <h4><?php echo __('End of container'); ?></h4>
	<p class="instructions"><?php echo __('You must have a Container Start block before this one.') ?></p>
</div>
