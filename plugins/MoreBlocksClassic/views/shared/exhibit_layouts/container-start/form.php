<?php
$formStem = $block->getFormStem();
$options = $block->getOptions();
?>
<div class="item-details">
    <h4><?php echo __('Container Options'); ?></h4>

    <div class="block-param">
        <?php echo $this->formLabel($formStem . '[options][class]', __('Container CSS Class:')); ?>
        <?php echo $this->formText($formStem . '[options][class]',@$options['class']); ?>
    </div>
	<p class="instructions"><?php echo __('You must have a Container End block after this one.') ?></p>
</div>

