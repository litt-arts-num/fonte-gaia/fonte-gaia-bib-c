<?php
$galleryImages = [];
foreach ($options['image'] as $img) {
    if (isset($img['url']) and ('' != $img['url'])) {
        if (isset($img['label'])) {
            $galleryImages[] = ["url" => html_escape($img['url']), "label" => $img['label']];
        } else {
            $galleryImages[] = ["url" => html_escape($img['url']), "label" => ''];
        }
    }
}
?>

<div class="custom-image-gallery wrapper">
<div class="custom-image-gallery container">
  <div class="row custom-gallery">
    <?php foreach ($galleryImages as $key => $value):?>

      <?php $unique_id = '_'.uniqid(); ?>

        <div class="col">
          <a class="exhibit-item-link custom-gallery" data-bs-toggle="modal" data-bs-target="<?= '#modal' . $unique_id; ?>">
              <img src="<?= $value['url'];?>" alt="<?= $value['label'];?>"/>
          </a>
          <div class="exhibit-item-caption">
              <p><?= $value['label'];?></p>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade custom-gallery" id="<?= 'modal' . $unique_id; ?>" tabindex="-1" aria-labelledby="<?= $value['label']; ?>" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

              <div class="modal-header">
                    <h5 class="modal-title" id="<?= 'modal' . $unique_id . '_Label';?>">
                          <?= $value['label'];?>
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                  <img src="<?= $value['url']; ?>" alt="<?= $value['label']; ?>"/>
              </div>

            </div>
          </div>
        </div>

      <?php endforeach; ?>

  </div>
</div>
</div>
