<?php
$formStem = $block->getFormStem();
$options = $block->getOptions();
?>
<div class="item-details">
    <h4><?php echo __('Image 1'); ?></h4>

    <div class="block-param">
        <?php echo $this->formLabel($formStem . '[options][image][0][url]', __('Image URL:')); ?>
        <?php echo $this->formText($formStem . '[options][image][0][url]', @$options['image']['0']['url']); ?>
    </div>

    <div class="block-param">
        <?php echo $this->formLabel($formStem . '[options][image][0][label]', __('Image Label:')); ?>
        <?php echo $this->formText($formStem . '[options][image][0][label]', @$options['image']['0']['label']); ?>
    </div>
    <p class="instructions"><?php echo __('Type your image URL.') ?></p>

    <h4><?php echo __('Image 2'); ?></h4>

    <div class="block-param">
        <?php echo $this->formLabel($formStem . '[options][image][1][url]', __('Image URL:')); ?>
        <?php echo $this->formText($formStem . '[options][image][1][url]', @$options['image']['1']['url']); ?>
    </div>

    <div class="block-param">
        <?php echo $this->formLabel($formStem . '[options][image][1][label]', __('Image Label:')); ?>
        <?php echo $this->formText($formStem . '[options][image][1][label]', @$options['image']['1']['label']); ?>
    </div>
    <p class="instructions"><?php echo __('Type your image URL.') ?></p>

    <h4><?php echo __('Image 3'); ?></h4>

    <div class="block-param">
        <?php echo $this->formLabel($formStem . '[options][image][2][url]', __('Image URL:')); ?>
        <?php echo $this->formText($formStem . '[options][image][2][url]', @$options['image']['2']['url']); ?>
    </div>

    <div class="block-param">
        <?php echo $this->formLabel($formStem . '[options][image][2][label]', __('Image Label:')); ?>
        <?php echo $this->formText($formStem . '[options][image][2][label]', @$options['image']['2']['label']); ?>
    </div>
    <p class="instructions"><?php echo __('Type your image URL.') ?></p>

    <h4><?php echo __('Image 4'); ?></h4>

    <div class="block-param">
        <?php echo $this->formLabel($formStem . '[options][image][3][url]', __('Image URL:')); ?>
        <?php echo $this->formText($formStem . '[options][image][3][url]', @$options['image']['3']['url']); ?>
    </div>

    <div class="block-param">
        <?php echo $this->formLabel($formStem . '[options][image][3][label]', __('Image Label:')); ?>
        <?php echo $this->formText($formStem . '[options][image][3][label]', @$options['image']['3']['label']); ?>
    </div>
    <p class="instructions"><?php echo __('Type your image URL.') ?></p>

<p>La gallerie s'adapte automatiquement au nombre d'images.</p>

</div>
