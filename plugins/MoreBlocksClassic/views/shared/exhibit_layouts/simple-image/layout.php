<?php
$image_url = isset($options['image_url'])
    ? html_escape($options['image_url'])
    : '';
?>
<img src="<?php echo $image_url; ?>" alt=""/>