<?php
$formStem = $block->getFormStem();
$options = $block->getOptions();
?>
<div class="item-details">
    <h4><?php echo __('Image'); ?></h4>

    <div class="block-param">
        <?php echo $this->formLabel($formStem . '[options][image_url]', __('Image URL:')); ?>
        <?php echo $this->formText($formStem . '[options][image_url]',@$options['image_url']); ?>
    </div>
	<p class="instructions"><?php echo __('Type your image URL.') ?></p>
</div>

