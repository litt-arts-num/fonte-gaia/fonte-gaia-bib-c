<?php
/**
 * @package MoreBlocksClassic
 */

class MoreBlocksClassicPlugin extends Omeka_Plugin_AbstractPlugin
{
    protected $_hooks = array(
        'install',
        'uninstall',
        'initialize',
        'admin_head',
        'public_head',
    );

    /**
     * @var array Filters for the plugin.
     */
    protected $_filters = array(
        'exhibit_layouts'
    );

    public function hookInstall($args)
    {
        // Any install functions
    }

    public function hookUninstall($args)
    {
        // Any uninstall functions
    }

    public function hookInitialize($args)
    {
        // Any initializing functions
    }

    public function hookAdminHead()
    {
        queue_css_file('more-blocks-classic-admin', 'all');
    }

    public function hookPublicHead()
    {
    }

    public function filterExhibitLayouts($layouts)
    {
        $layouts['container-start'] = array(
            'name' => 'Container Start',
            'description' => 'Start of container with optional CSS class.'
        );
        $layouts['container-end'] = array(
            'name' => 'Container End',
            'description' => 'End of container.'
        );
        $layouts['simple-image'] = array(
            'name' => 'Image',
            'description' => 'Simple image from url.'
        );
        $layouts['image-gallery'] = array(
            'name' => 'Image-Gallery',
            'description' => 'Custom images gallery.'
        );
        return $layouts;
    }
}
