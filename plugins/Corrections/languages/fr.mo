��    %      D  5   l      @     A     V  %   h     �     �     �     �  J   �  I        b     k  
   t       �   �     <  G   I     �     �  `   �  )   �  T   &     {     �     �     �     �  
   �     �  	   �  .   �  3     1   E     w  }   �  =     6   C  m  z     �	     
  $   
      ?
     `
     i
     u
  Y   �
  a   �
     O  	   \  
   f     q  �   �     K  ;   ]     �     �  >   �  $   �  f        l     �     �     �     �  
   �     �     �  *   �  1     &   4     [  �   n  ^     A   l                    
       $                                        "             #            !         %                                                          	                    (Allowed To Contact) (May Not Contact) A correction has been submitted to %s Accept correction for "%s" Accepted Added: Can we contact you? Check the metadata fields that you want to make correctable by the public. Check this box if it is okay for us to contact you about this correction. Comment: Comments Correction Corrections Corrections are additive. No data will be deleted or replaced. After accepting a
        correction, review it on the item page and edit there to confirm the decision.
         Default text Email address to be notified about corrections (defaults to site admin) From: On: Please describe the nature of this correction, or anything about it that we should know. Thanks! Please see %s to evaluate the correction. ReCaptcha is not configured. Anonymous users will not be able to submit corrections. Reject correction for "%s" Rejected Reviewed on: Status Status: Status: %s Submit Correction Submitted Text for invitation to suggesting a correction Thank you for taking the time to improve this site! Thank you for the correction. It is under review. View correction You can also leave general comments or suggestions in the "comments" section. An administrator will review your contribution. You can suggest corrections to the following fields for item  Your CAPTCHA submission was invalid, please try again. Project-Id-Version: Corrections
Report-Msgid-Bugs-To: http://github.com/patrickmj/Corrections/issues
PO-Revision-Date: 2021-03-01 10:37+0100
Last-Translator: Camille <camille@camille-latitude-5500>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 (Autoriser à être contacté) (Ne pas contacter) Une correction a été soumise à %s Accepter la correction pour "%s" Accepté Ajouté le: Pouvons-nous vous contacter ? Vérifier les champs de métadonnées que vous souhaitez que les contributeurs corrigent. Cocher cette case, si vous êtes d'accord pour qu'on vous contacte à propos de cette correction. Commentaire: Commentez Correction Les corrections Les corrections sont complémentaires. Aucune donnée ne sera supprimée ou remplacée. Après accepter un
correction, vérifiez-la sur la page de l'article et modifiez-la pour confirmer ladécision.
  Texte par défaut Adresse mail qui recevera les notifications des corrections De: Le: Pourriez vous décrire la nature de votre correction, merci !  Voir %s pour évaluer la correction. La ReCaptcha n'est pas configuré. Les contributeurs anonyme ne pourront pas soummetre de corrections. Rejeter la correction pour "%s" Rejeté Revu le: Statut Statut: Statut: %s Soumettre une correction Soumis Texte invitant à suggérer une correction Merci d'avoir aidé à l'amélioration de ce site Merci pour votre correction. On relit. Voir le collection Vous pouvez également laisser un commentaire général ou une suggestion dans la section "commentaires". Un des administrateurs regardera votre contribution. Vous pouvez suggérer une correction pour les champs de métadonnées suivantes de ce contenu. La soumission de votre CAPTCHA est invalide, veuillez réessayer. 