<?php
queue_css_file('correction');
echo head(array('title' => 'Corrections','bodyclass' => 'corrections'));
$user = current_user();
?>
<?php echo flash(); ?>


<div class="breadcrumb"><div class="wrap wrap--xl"><p id="simple-pages-breadcrumbs"><?php echo link_to_home_page(__('Home')); ?> > <?php echo link_to_collection_for_item(); ?> > <span><?php echo metadata('item', 'display_title'); ?></span></p></div></div>

<div class="wrap wrap--SM item-contents">
	<h1 class="page__title"><?php echo __('Suggestions for item '); ?><?php echo metadata($item, array('Dublin Core', 'Title')); ?></h1>
	<p>	
	<?php echo __('You can suggest corrections to the following fields for item '); ?>
	<?php echo link_to($item, 'show', metadata($item, array('Dublin Core', 'Title'), array('no_filter' => true)) . '.'); ?>
	</p>
	<p>
	<?php echo __('You can also leave general comments or suggestions in the "comments" section. An administrator will review your contribution.'); ?>
	</p>
	<p>
	<?php echo __('Thank you for taking the time to improve this site!'); ?>
	</p>

	<form method='post' class="wrap wrap--xs">


	<div class="field">
		<div class="">
			<label for='comment'><?php echo __('Comments'); ?></label>
		</div>
		<div class="inputs">
			<p class="explanation"><?php echo __('Please describe the nature of this correction, or anything about it that we should know. Thanks!'); ?></p>
			<div class="input-block">
				<textarea cols='50' rows='3' name='comment'></textarea>
			</div>
		</div>
	</div>

	<?php if ( ! $user): ?>

	<div class="field">
		<div class="">
			<label for='email'><?php echo __('Email'); ?></label>
		</div>
		<div class="inputs">
			<p class="explanation"></p>
			<div class="input-block">
				<input type='text' name='email' />
			</div>
		</div>
	</div>

	<?php endif; ?>


	<div class="field">
		<div class="">
			<label for='may_contact'><?php echo __('Can we contact you?'); ?></label>
		</div>
		<div class="inputs">
			<p class="explanation"><?php echo __('Check this box if it is okay for us to contact you about this correction.'); ?></p>
			<div class="input-block">
				<input type='checkbox' value='1' name='may_contact' />
			</div>
		</div>
	</div>


	<?php
	foreach ($elements as $element) {
		echo "<div class='element-correction' >";
		$elName = $element->name;
		$elSet = $element->getElementSet();
		$elSetName = $elSet->name;
		echo $this->elementForm($element, $corrections_correction);
		echo "<div class='correction-current-data'><p class='correction-current-data__label'>" . __('Current data for %s', $elName) . "</p>";
		echo "<p class='correction-current-data__content'>" . metadata($item, array($elSetName, $elName), array('no_filter' => true)) . "</p>";
		echo "</div></div>";
	}
	?>


	<?php
	if (! $user) {
		echo $captchaScript;
	}
	?>
	<div class='correction-submit'><?php echo $this->formSubmit('submit', __('Submit Correction'));	?></div>

	<input type='hidden' name='item_id' value='<?php echo $item->id; ?>' />
	</form>
</div>
<?php echo foot(); ?>