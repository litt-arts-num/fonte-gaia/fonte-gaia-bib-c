<?php echo head(array('bodyid'=>'home', 'bodyclass' =>'two-col')); ?>


<?php
	
	// variable pour modifier l'ordre des sections si pas de carrousel
	$order_section = 'order_change';

	if( get_theme_option('homeslider_check') == 1):

		$order_section = 'order_normal';
?>
<section class="section-home section-home--slider">

	<div class="slider splide" id="home-slider">
		<div class="splide__arrows">
			<button class="splide__arrow splide__arrow--prev">
				Prev
			</button>
			<button class="splide__arrow splide__arrow--next">
				Next
			</button>
		</div>
		<div class="splide__track">
			<div class="splide__list">
		<?php

		for($numSlide=1; $numSlide<=4; $numSlide++):

			$SlideImage = get_theme_option('homeslide_img'.$numSlide);

			if ($SlideImage):
			
				$SlideTitle = get_theme_option('homeslide_title'.$numSlide);
				$SlideText = get_theme_option('homeslide_text'.$numSlide);
				$SlideButton = get_theme_option('homeslide_button'.$numSlide);
				$SlideLink = get_theme_option('homeslide_link'.$numSlide);

				
				if ($SlideImage):
					$SlideImage = '<img src="/files/theme_uploads/'.$SlideImage.'" alt="">';
				/*else:
					$SlideImage = '<img src="'.html_escape(img('default-slide-image.jpg')).'" alt="">';*/
				endif;

				?>
				<div class="slider__contents splide__slide">
					<div class="slider__image">
						<?php echo $SlideImage; ?>
					</div>
					<div class="slider__content">
						<h2 class="slider__title"><?php echo $SlideTitle; ?></h2>
						<div class="slider__description"><?php echo $SlideText; ?></div>
						<?php if ($SlideButton && $SlideLink): ?>
							<p class="button--container"><a class="button" href="<?php echo $SlideLink; ?>"><?php echo $SlideButton; ?></a></p>
						<?php endif; ?>
					</div>
				</div>
				<?php

			endif;

		endfor;
		?>
			</div>
		</div>
	</div>
</section><!-- end section-home--slider -->
<script>
	document.addEventListener( 'DOMContentLoaded', function () {
		var splide1 = new Splide( '#home-slider', {
			arrows: true,
			keyboard: true,
			pagination: true,
			type: 'loop',
			autoplay: true,
			interval: 8000,
		} );
		// reference https://splidejs.com/guides/overflow/#loop-carousel
		splide1.on( 'overflow', function ( isOverflow ) {
			// Reset the carousel position
			splide1.go( 0 );

			splide1.options = {
				arrows    : isOverflow,
				pagination: isOverflow,
				drag      : isOverflow,
				clones    : isOverflow ? undefined : 0, // Toggle clones
			};
		} );
		splide1.mount();
	} );
</script>

<?php
	endif;
?>

<?php if ( $readingpathText = get_theme_option('homepage_readingpath_intro') ): ?>
<section class="section-home home-readingpath">

	<h2 class="section-home__title home-readingpath__title"><?php echo __('Reading paths'); ?></h2>
	<div class="home-readingpath__description">
		<?php echo $readingpathText; ?>
		<?php 
			if ( $readingpathList = get_theme_option('homepage_readingpath_list') ):
			echo $readingpathList; 
			endif;
		?>
	</div>


	<?php if( get_theme_option('homepage_readingpath_img1') && get_theme_option('homepage_readingpath_url1') && get_theme_option('homepage_readingpath1')) : ?>
	<div class="home-readingpath__list wrap">
		<?php
		$aleaIndex = range(1, 6);
		shuffle($aleaIndex);

		for($numPath=0; $numPath<=5; $numPath++):

			$PathImage = get_theme_option('homepage_readingpath_img'.$aleaIndex[$numPath]);

			if ($PathImage):
			
				$PathTitle = get_theme_option('homepage_readingpath'.$aleaIndex[$numPath]);
				$PathLink = get_theme_option('homepage_readingpath_url'.$aleaIndex[$numPath]);

				
				if ($PathImage):
					$PathImage = '<img src="/files/theme_uploads/'.$PathImage.'" alt="">';
				endif;

				?>
				<div class="home-readingpath__item">
					<div class="home-readingpath__img">
						<?php echo $PathImage; ?>
					</div>
					<a href="<?php echo $PathLink; ?>"><h3><?php echo $PathTitle; ?></h3></a>
				</div>
				<?php

			endif;

		endfor;
		?>
	</div>
	<?php endif; ?>

</section><!-- .readingpath -->
<?php endif; ?>


<?php if ($introText = get_theme_option('homepage_intro_text')): ?>
<section class="section-home home-intro wrap wrap--lg <?php echo $order_section; ?>">

	<div class="wrap">
		<?php 
		$introImage = get_theme_option('homepage_intro__img');

		if ($introImage):
			$introImage = '<img src="/files/theme_uploads/'.$introImage.'" alt="">';

			echo '<div class="home-intro__img">'.$introImage.'</div>';
		endif;
		?>
		<div class="home-intro__content">
			<?php if ( $introTitle =get_theme_option('homepage_intro_title')): ?>
				<h2 class="section-home__title home-intro__title"><?php echo $introTitle; ?></h2>
			<?php endif; ?>

			<div class="home-intro__description"><?php echo $introText; ?></div>

			<?php if ( ($introLink = get_theme_option('homepage_intro_link')) && ($introLinkURL = get_theme_option('homepage_intro_linkurl')) ): ?>
				<p class="home-intro__link">
					<a href="<?php echo $introLinkURL; ?>" class="button"><?php echo $introLink; ?></a>
				</p>
			<?php endif; ?>
		</div>
	</div>
		
</section>
<?php endif; ?><!-- .section-home--intro -->

<section class="section-home home-collections">
	<div class="wrap wrap--lg">
	<div class="wrap">

		<div class="home-collections__title"><h2 class="section-home__title"><?php echo __('Collections'); ?></h2></div>

		<?php
		if ( $collectionToDisplay = get_theme_option('homepage_collection') ):
			$collectionTable = get_records('Collection', array('range'=>$collectionToDisplay) );
		else:
			$collectionTable = get_recent_collections( 6 );
		endif;
		?>
		<ul class="collection__list">
		<?php foreach ($collectionTable as $collection): ?>


			<li class="collection__item">
				<div class="collection__img">
					<?php
					$collectionImage = 'collections/collection_id'.$collection->id.'.png';

					if ( file_exists( dirname(__FILE__).'/images/'.$collectionImage) ):
						
						echo '<img src="'.img($collectionImage).'">';

					elseif( $collectionImage = get_theme_option('homepage_collection_default_img')):
						$collectionImage = '<img src="/files/theme_uploads/'.$collectionImage.'" alt="">';

						echo $collectionImage;
					endif;
					?>
				</div>
					
				<h3 class="collection__title">
					<?php echo link_to_collection(metadata($collection, 'display_title'),array(),'show', $collection); ?>
				</h3>
			</li>
		<?php endforeach; ?>
		</ul>

		<p class="button--container"><a href="/collections/browse" class="button"><?php echo __('All the collections'); ?></a></p>
	</div>
	</div>

</section><!-- end section-home--collection -->

<section class="section-home home-recent">
	<div class="wrap wrap--lg">
	<?php

	$recents = get_recent_items();
	//var_dump($recents);

	$recentItems = get_theme_option('homepage_recent_items');
	if ($recentItems === null || $recentItems === ''):
		$recentItems = 3;
	else:
		$recentItems = (int) $recentItems;
	endif;
	if ($recentItems):
	?>
	<?php

		set_loop_records('items', get_recent_items($recentItems));  
		if (has_loop_records('items')):
	?>
		<h2 class="section-home__title"><?php echo __('News'); ?></h2>

		<div class="slider splide" id="recent-slider">
			<div class="splide__arrows">
				<button class="splide__arrow splide__arrow--prev">
					Prev
				</button>
				<button class="splide__arrow splide__arrow--next">
					Next
				</button>
			</div>
			<div class="splide__track">
				<div class="splide__list">

				<?php foreach (loop('items') as $item): ?>
				<div class="slider__contents splide__slide">
					
					<div class="slider__image">
						<?php if (metadata($item, 'has files')): ?>
							<?php echo link_to_item(item_image('original')); ?>

						<?php elseif( $itemImage = get_theme_option('item_default_img')):
						$itemImage = '<img src="/files/theme_uploads/'.$itemImage.'" alt="">';

							echo $itemImage;
						endif; ?>
					</div>

					<div class="slider__content">
						<?php                           
							$itemShortTitle = metadata('item', 'display_title', array('snippet' => 80));
						?>
						<h3 class="slider__title"><?php echo link_to_item($itemShortTitle); ?></h3>

						<?php $author = metadata($item, array('Dublin Core', 'Creator')); ?>
						<?php if ($author): ?>
							<span class="slider__author"><span><?php echo __('Author: '); ?></span><?php echo $author; ?></span>
						<?php endif; ?> 

						<?php $date = metadata($item, array('Dublin Core', 'Date')); ?>
						<?php if ($date): ?>
							<span class="slider__date"><span><?php echo __('Date: '); ?></span><?php echo $date; ?></span>
						<?php endif; ?> 

						<?php $description = metadata($item, array('Dublin Core', 'Description'), array('snippet' => 120)); ?>
						<?php if ($date): ?>
							<div class="slider__description"><?php echo $description; ?></div>
						<?php endif; ?>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>  

		<?php endif; ?>
	<?php endif; ?>
	</div>

</section><!-- end section-home--recent -->

	<?php //fire_plugin_hook('public_home', array('view' => $this)); ?>

<script>
	document.addEventListener( 'DOMContentLoaded', function () {
		
		var splide2 = new Splide( '#recent-slider', {
			arrows: true,
			keyboard: true,
			pagination: true,
			type: 'loop',
			autoplay: true,
			interval: 8000,
			perPage: 3,
			breakpoints: {
				599: {
					perPage: 1,
				},
				1079: {
					perPage: 2,
				}
			}
		} );
		splide2.on( 'overflow', function ( isOverflow ) {
			// Reset the carousel position
			splide2.go( 0 );

			splide2.options = {
				arrows    : isOverflow,
				pagination: isOverflow,
				drag      : isOverflow,
				clones    : isOverflow ? undefined : 0, // Toggle clones
			};
		} );

		splide2.mount();
	} );
</script>



<?php echo foot(); ?>
