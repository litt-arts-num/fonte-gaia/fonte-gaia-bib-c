<?php $collectionMetadataToHide = 
	array(
		'dublin-core-title',
		'dublin-core-description',
	);
?>
<?php foreach ($elementsForDisplay as $setName => $setElements): ?>
<div class="element-set">
    <?php if ($showElementSetHeadings): ?>
    <h2><?php echo html_escape(__($setName)); ?></h2>
    <?php endif; ?>
    <?php foreach ($setElements as $elementName => $elementInfo): ?>

	<?php if ( !in_array( text_to_id(html_escape("$setName $elementName")), $collectionMetadataToHide) ): ?>
    <div id="<?php echo text_to_id(html_escape("$setName $elementName")); ?>" class="element">
        <h3><?php echo html_escape(__($elementName)); ?></h3>
        <?php foreach ($elementInfo['texts'] as $text): ?>
            <div class="element-text"><?php echo $text; ?></div>
        <?php endforeach; ?>
    </div><!-- end element -->
    <?php endif; ?>

    <?php endforeach; ?>
</div><!-- end element-set -->
<?php endforeach;