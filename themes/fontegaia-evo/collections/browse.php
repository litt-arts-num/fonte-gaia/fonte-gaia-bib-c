<?php
$pageTitle = __('Browse Collections');
echo head(array('title'=>$pageTitle,'bodyclass' => 'collections browse'));
?>

<div class="wrap">
	<h1 class="page__title"><?php echo $pageTitle; ?> <?php echo __('(%s total)', $total_results); ?></h1>

	<div class="collections-list">
	
	<?php foreach (loop('collections') as $collection): ?>

	<div class="collection">

		<div class="collection__img">
			<?php 

				$collectionSubstitute = 'collections/collection_id'.$collection->id.'.png';

			if( file_exists( dirname(__FILE__).'/../images/'.$collectionSubstitute) ): 

				echo '<img src="'.img($collectionSubstitute).'">';

			elseif($collectionImage = record_image('collection')): 

				echo $collectionImage; 

			elseif( $collectionSubstitute = get_theme_option('itemset_default_img')):

				$collectionSubstitute = '<img src="/files/theme_uploads/'.$collectionSubstitute.'" alt="">';
				echo $collectionSubstitute;

			endif;
			?>
		</div>
			
		
		
		<h2><?php echo link_to_collection(); ?></h2>

	</div><!-- end class="collections" -->

	<?php endforeach; ?>

	</div>

	<?php fire_plugin_hook('public_collections_browse', array('collections'=>$collections, 'view' => $this)); ?>
</div>

<?php echo foot(); ?>
