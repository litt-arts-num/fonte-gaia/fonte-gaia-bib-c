<?php
$collectionTitle = metadata('collection', 'display_title');
$totalItems = metadata('collection', 'total_items');
?>

<?php echo head(array('title' => $collectionTitle, 'bodyclass' => 'collections show')); ?>

<div class="wrap">
	<?php
        $description = metadata('collection', array('Dublin Core', 'Description'));
        $class_descr = '';

        if ($description):
            $class_descr = ' collection__header--description';
        endif;
    ?>

	<div class="collection__header<?php echo $class_descr; ?> wrap wrap--sm">
		<h1 class="page__title"><?php echo metadata('collection', 'rich_title', array('no_escape' => true)); ?></h1>
		<div class="collection__img">
			<?php

            $collectionSubstitute = 'collections/collection_id'.$collection->id.'.png';

            if (file_exists(dirname(__FILE__).'/../images/'.$collectionSubstitute)):

                echo '<img src="'.img($collectionSubstitute).'">';

            elseif ($collectionImage = record_image('collection')):

                echo $collectionImage;

            elseif ($collectionSubstitute = get_theme_option('itemset_default_img')):

                $collectionSubstitute = '<img src="/files/theme_uploads/'.$collectionSubstitute.'" alt="">';
                echo $collectionSubstitute;

            endif;
            ?>
		</div>

		<?php if ($description): ?>
			<div class="collection__description">
				<p><?php echo $description; ?></p>
			</div>
		<?php endif; ?>
	</div>

	<?php $collectionMetadataHtml = all_element_texts('collection', array( 'show_element_set_headings'=>false, 'partial'=>'collections/collection-metadata.php' )); ?>
	<?php if (strpos($collectionMetadataHtml, 'class="element"')): ?>
	<div class="collection__informations accordion-container generic-accordion">
		<div class="accordion-header inactive-header">
			<h2 class="accordion-title">Informations sur la collection</h2>
		</div>
		<div class="accordion-content">
			<?php  echo $collectionMetadataHtml; ?>
		</div><!-- .accordion-content -->
	</div><!-- .collection__information.accordion-container -->
	<?php endif; ?>

	<div id="collection-items">
		<h2 class="items-list__title"><?php echo __('Collection Items'); ?></h2>
		<?php if ($totalItems > 0): ?>

			<div class="items-list">
				<?php foreach (loop('items') as $item): ?>
				<?php $itemTitle = metadata('item', 'display_title'); ?>
				<div class="item hentry">

					<div class="item__img">
						<?php
                        if (metadata('item', 'has thumbnail')):

                            echo link_to_item(item_image(null, array('alt' => $itemTitle)));

                        elseif ($collectionSubstitute = get_theme_option('item_default_img')):

                            echo '<img src="/files/theme_uploads/'.$collectionSubstitute.'" alt="">';

                        endif;
                        ?>
					</div>

					<div class="item__content">
						<h3 class="item__title"><?php echo link_to_item($itemTitle, array('class' => 'permalink')); ?></h3>
						<?php if ($author = metadata('item', array('Dublin Core', 'Creator'))): ?>
						<div class="item-author">
							<?php echo '<span class="label">'.__('Author'). ' : ' .'</span>'; ?><?php echo $author; ?>
						</div>
						<?php endif; ?>
						<?php if ($publisher = metadata('item', array('Dublin Core', 'Publisher'))): ?>
						<div class="item-publisher">
							<?php echo '<span class="label">'.__('Publisher'). ' : ' .'</span>'; ?><?php echo $publisher; ?>
						</div>
						<?php endif; ?>
						<?php if ($date = metadata('item', array('Dublin Core', 'Date'))): ?>
						<div class="item-date">
							<?php echo '<span class="label">'.__('Date'). ' : ' .'</span>'; ?><?php echo $date; ?>
						</div>
						<?php endif; ?>

						<?php /* if ($description = metadata('item', array('Dublin Core', 'Description'), array('snippet' => 250))): ?>
                        <div class="item-description">
                            <?php echo $description; ?>
                        </div>
                        <?php endif; */ ?>
					</div>

				</div>
				<?php endforeach; ?>
			</div>

			<a class="button" style="margin-top:10px;" href="/solr-search?q=&facet=collection%3A<?php echo urlencode('"'.$collectionTitle.'"');?>" class="view-items-link"><?php echo __('View all %s items', $totalItems);  /* echo __(plural('View item', 'View all %s items', $totalItems), $totalItems); */?></a>

		<?php else: ?>
			<p><?php echo __("There are currently no items within this collection."); ?></p>
		<?php endif; ?>
	</div><!-- end collection-items -->

	<?php fire_plugin_hook('public_collections_show', array('view' => $this, 'collection' => $collection)); ?>
</div>

<?php if (strpos($collectionMetadataHtml, 'class="element"')): ?>
<script type="text/javascript">
	// Script accordeon infos collection
	jQuery( document ).ready( function( $ ) {
		$('.accordion-header').click(function () {
			if($(this).is('.inactive-header')) {
				$(this).toggleClass('active-header').toggleClass('inactive-header');
				$(this).next().slideToggle().toggleClass('open-content');
			}
			else {
				$(this).toggleClass('active-header').toggleClass('inactive-header');
				$(this).next().slideToggle().toggleClass('open-content');
			}
		});
		return false;
	});
</script>
<?php endif; ?>

<?php echo foot(); ?>
