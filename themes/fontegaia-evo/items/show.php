<?php echo head(array('title' => metadata('item', array('Dublin Core', 'Title')),'bodyclass' => 'item show')); ?>

<div class="breadcrumb"><div class="wrap wrap--xl"><p id="simple-pages-breadcrumbs"><?php echo link_to_home_page(__('Home')); ?> > <?php echo link_to_collection_for_item(); ?> > <span><?php echo metadata('item', 'display_title'); ?></span></p></div></div>

<div class="wrap wrap--xl item-contents">

	<h1 class="item__title"><?php echo metadata('item', array('Dublin Core', 'Title')); ?></h1>

	<?php
        if ($this->universalViewer($item) == ''):
            $itemWhithoutMedia = true;
        else:
            $itemWhithoutMedia = false;
        endif;
    ?>

	<div class="item-contents__main-block<?php echo(($itemWhithoutMedia) ? ' item-contents__main-block--nomedia':''); ?>">

		<?php
            $itemIdentifier = metadata('item', array('Dublin Core', 'Identifier'));

            if (($itemWhithoutMedia) && (strncmp($itemIdentifier, 'http', 4) === 0)):
                $externalMediaLink = $itemIdentifier;
            else:
                $externalMediaLink = false;
            endif;
        ?>
		<div class="item-contents__viewer">
			<?php if (!$itemWhithoutMedia): // Lecteur média?>
			<div id="item-viewer">
				<?php
                    if (plugin_is_active('UniversalViewer')) {
                        echo get_specific_plugin_hook_output('UniversalViewer', 'public_items_show', array('view' => $view, 'item' => $item));
                    }
                ?>
			</div>
			<?php elseif ($externalMediaLink): // Media externe?>
			<div id="item-viewer" class="item-external-media-link">
				<p class="message"><?php if ($message = get_theme_option('external_media_items')): echo $message; else: echo __('Document unavailable'); endif; ?></p>
				<a href="<?php echo $externalMediaLink; ?>" target="_blank" class="button"><?php echo __('See the scanned document'); ?></a>
			</div>
			<?php else: // Aucun media?>
			<div id="item-viewer" class="item-no-media">
				<p class="message"><?php if ($message = get_theme_option('nomedia_items')): echo $message; else: echo __('Document unavailable'); endif; ?></p>
			</div>
			<?php endif; ?>
			<?php

                $data_rights = metadata('item', array('Dublin Core', 'Rights'));
                $data_source = metadata('item', array('Dublin Core', 'Source'));
                //$data_pdf = ;

            if (($data_rights) || (metadata('item', array('Dublin Core', 'Source')))): ?>
			<div class="item-contents__rights">

				<?php if ($data_rights): ?>
					<div class="item__rights">
						<strong class="item__label"><?php echo __('Rights'); echo __(':'); ?></strong>
						<?php echo $data_rights; ?>
					</div>
				<?php endif; ?>

				<?php if ($data_source): ?>
					<div class="item__source">
						<strong class="item__label"><?php echo __('Source'); echo __(':');; ?></strong>
						<?php echo $data_source; ?>
					</div>
				<?php endif; ?>


				<?php // Link To PDF
                    $CurrentItemFiles = $item->Files;
                    foreach ($CurrentItemFiles as $file) {
                        if ($file["mime_type"] == "application/pdf") {
                            $pdfFile = $file;
                            break;
                        }
                    }
                    ?>
				<?php if ($pdfFile): ?>
					<div class="item__file">
						<strong class="item__label"><?php echo __('PDF'); echo __(':');; ?></strong>
								 <a href="<?php echo file_display_url($pdfFile, 'original'); ?>" target="_blank" rel="noopener noreferrer" ><?php echo __('Télécharger le document complet en pdf');?></a>
					</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>

		<div class="item-contents__general right-panel">

			<button class="button__right-panel"><span class="button-open"><?php echo __('Show notice'); ?></span><span class="button-close"><?php echo __('Close'); ?></span></button>
			<div class="item-contents__general-container">
				<div class="item-contents__general-panel">

						<?php echo all_element_texts('item', array('partial' => 'common/itemMetadataBlock.php')); ?>

			 </div>
				<div class="item-contents__correction-panel">
					<?php  	 if (plugin_is_active('Corrections')) :
                       if (current_user() != null): ?>
						<div class="item-contents__block">
							<p><?php echo __('You noticed an error?'); ?></p>
							<p><a href="/corrections/index/add/item_id/<?php echo $item->id; ?>" class="button"><?php echo __('Submit your correction'); ?></a></p>
						</div>
					<?php else: ?>
						<div class="item-contents__block">
							<p><?php echo __('You noticed an error?'); ?></p>
							<p><a style="max-width:280px;" href="/users/login" class="button"><?php echo('Connectez-vous pour proposer une correction'); ?></a></p>
						</div>
					<?php endif; ?>
				<?php endif; ?>


				</div>
			</div>


		</div>

	</div>

	<?php
    $itemTypeMetadataKeys = array( 'Thématique', 'Genre', 'Type de support', 'Projet de recherche' );
    $itemTypeMetadata = item_type_elements();
    $itemTypeMetadataToDisplay = array();

    foreach ($itemTypeMetadataKeys as $itemTypeMetadataKey) :
        if ($itemTypeMetadata[$itemTypeMetadataKey] !== null) {
            $itemTypeMetadataToDisplay[] = $itemTypeMetadata[$itemTypeMetadataKey];
        }
    endforeach;

    if ($itemTypeMetadataToDisplay):
    ?>
	<div class="item-contents__aside">
		<h2><?php echo __('See also'); ?></h2>
		<?php echo implode(" • ", $itemTypeMetadataToDisplay); ?>
	</div>
	<?php endif; ?>

	<div class="wrap wrap--lg">
		<?php
            /* COMMENTAIRE : appel global des modules en automatique via le fire_plugin désactivé car provoque l'appel d'universal viewer en double. Appel d'autres modules à mettre manuellement. */
            //fire_plugin_hook('public_items_show', array('view' => $this, 'item' => $item));

        ?>
		<?php

        if (plugin_is_active('Contribution')) : ?>
			<div class="wrap wrap--xs item-contents__plugin">
				<p class="h3"><?php echo __('You want to propose a new content?'); ?></p>
				<p><a href="/contribution" class="button"><?php echo __('Submit your content'); ?></a></p>
			</div>
		<?php endif; ?>
	</div>

	<!-- <nav>
		<ul class="item-pagination navigation">
			<li id="previous-item" class="previous"><?php echo link_to_previous_item_show(); ?></li>
			<li id="next-item" class="next"><?php echo link_to_next_item_show(); ?></li>
		</ul>
	</nav> -->

</div>
<script type="text/javascript">
	// toggle right panel -- metadatas
	document.querySelector('.button__right-panel').onclick = function() {
		document.querySelector('.item-contents__main-block').classList.toggle('right-panel-open');
	};
</script>
<?php echo foot(); ?>
