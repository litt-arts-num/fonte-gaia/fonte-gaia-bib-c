<?php
$records = array();
foreach ($attachments as $attachment) {
    $item = $attachment->getItem();
    if ($item) {
        $records[] = $item;
    }
}
// Display the viewer with the specified item.
echo $this->universalViewer($records[0]);
// This is hacked : cannot get a list to function for now, so I restrict to the first item : records[0]
