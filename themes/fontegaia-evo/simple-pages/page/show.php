<?php
$bodyclass = 'page simple-page';
if ($is_home_page):
	$bodyclass .= ' simple-page-home';
endif;

echo head(array(
	'title' => metadata('simple_pages_page', 'title'),
	'bodyclass' => $bodyclass,
	'bodyid' => metadata('simple_pages_page', 'slug')
));
?>

<div id="primary">
	<div class="wrap">
		<?php if (!$is_home_page): ?>
		<p id="simple-pages-breadcrumbs"><?php echo simple_pages_display_breadcrumbs(); ?></p>
	</div>
	<article>
		<div class="wrap wrap--sm">
			<h1 class="page__title"><?php echo metadata('simple_pages_page', 'title'); ?></h1>
			<?php endif; ?>
			<?php
			$text = metadata('simple_pages_page', 'text', array('no_escape' => true));
			echo $this->shortcodes($text);
			?>
		</div>
	</article>
		
</div>

<?php echo foot(); ?>
