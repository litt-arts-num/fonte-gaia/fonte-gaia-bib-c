<?php
queue_js_file('guest-user-password');
queue_css_file('skeleton');
$css = "form > div { clear: both; padding-top: 10px;}";
queue_css_string($css);
$pageTitle = get_option('guest_user_register_text') ? get_option('guest_user_register_text') : __('Register');
echo head(array('bodyclass' => 'register', 'title' => $pageTitle));
?>
<div class="wrap wrap--lg breadcrumb">
	<p id="simple-pages-breadcrumbs"><?php echo link_to_home_page(__('Home')); ?> > <?php echo $pageTitle; ?></p>
</div>

<main class="wrap wrap--lg">
	<h1 class="page__title"><?php echo $pageTitle; ?></h1>

	<div class="wrap wrap--xs">
		<div id='primary'>
		<div id='capabilities'>
		<p>
		<?php echo get_option('guest_user_capabilities'); ?>
		</p>
		</div>
		<?php echo flash(); ?>
		<?php echo $this->form; ?>
		<p id='confirm'></p>
		</div>
	</div>
</main>
<?php echo foot(); ?>
