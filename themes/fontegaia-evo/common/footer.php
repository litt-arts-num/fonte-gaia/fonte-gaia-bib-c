	</div><!-- end content -->


	<footer role="contentinfo" class="footer">
		<div class="wrap wrap--xl">
			<div class="footer__partners">
				<div class="footer__partners__list">

				<?php
				for($numPartner=1; $numPartner<=13; $numPartner++):
				
					$PartnerImage = get_theme_option('footer_partner_logo'.$numPartner);
					$PartnerAlt = get_theme_option('footer_partner_alt'.$numPartner);
					$PartnerUrl = get_theme_option('footer_partner_url'.$numPartner);

					if ($PartnerImage && $PartnerAlt):

						if($PartnerUrl):
							$Partner = '<a href="'.$PartnerUrl.'"><img src="/files/theme_uploads/'.$PartnerImage.'" alt="'.$PartnerAlt.'"></a>';
						else:

							$Partner = '<img src="/files/theme_uploads/'.$PartnerImage.'" alt="'.$PartnerAlt.'">';
						endif;
					?>
					<span class="footer__partner">
						<?php echo $Partner; ?>
					</span>

					<?php 	
					endif;
				endfor; 
				?>
			</div>
			<?php 
				$partners_label = get_theme_option('footer_partners_label');
				$partners_url = get_theme_option('footer_partners_url');
			if ($partners_label && $partners_url): 
				?>
			<div class="footer__partners__link">
				<a href="<?php echo $partners_url; ?>" class="button"><?php echo $partners_label; ?></a>
			</div>
			<?php endif; ?>

			</div>
			<div class="footer__menu">
				<?php echo get_theme_option('footer_menu'); ?>

				<?php if ( $copyright = get_theme_option('footer_copyright')): ?>
					<p class="footer__copyright"><?php echo '&copy;'.date('Y').' - '.$copyright; ?></p>
				<?php endif; ?>
			</div>

			<?php fire_plugin_hook('public_footer', array('view' => $this)); ?>

		</div>
	</footer><!-- end footer -->

	<script type="text/javascript">
	jQuery(document).ready(function () {
		Omeka.showAdvancedForm();
		Omeka.skipNav();
		Omeka.megaMenu('.no-touchevents #primary-nav');
		FonteGaia.mobileMenu();
	});
	</script>

</body>
</html>
