<!DOCTYPE html>
<html lang="<?php echo get_html_lang(); ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php if ($author = option('author')): ?>
	<meta name="author" content="<?php echo $author; ?>" />
	<?php endif; ?>
	<?php if ($copyright = option('copyright')): ?>
	<meta name="copyright" content="<?php echo $copyright; ?>" />
	<?php endif; ?>
	<?php if ($description = option('description')): ?>
	<meta name="description" content="<?php echo $description; ?>" />
	<?php endif; ?>
	<?php
    if (isset($title)) {
        $titleParts[] = strip_formatting($title);
    }
    $titleParts[] = option('site_title');
    ?>
	<title><?php echo implode(' &middot; ', $titleParts); ?></title>

	<?php echo auto_discovery_link_tags(); ?>

	<!-- Plugin Stuff -->

	<?php fire_plugin_hook('public_head', array('view'=>$this)); ?>


	<!-- Stylesheets -->
	<?php
    if ($bodyid == 'home') {
        queue_css_file('../lib/splide/css/splide.min');
    } // Splide on homepage
        queue_css_file('bootstrap/bootstrap.min');
    queue_css_file(array('iconfonts','style'));
    echo head_css();

    echo theme_header_background();

    if (!current_user()):
    ?>
	<style type="text/css">
		#admin-bar { display: none;}
		#admin-bar li a[href*='lang'] { display: none; }
	</style>

	<?php
    endif;
    ?>

	<?php

    ($backgroundColor = get_theme_option('background_color')) || ($backgroundColor = "#FFFFFF");
    ($textColor = get_theme_option('text_color')) || ($textColor = "#444444");
    ($linkColor = get_theme_option('link_color')) || ($linkColor = "#888888");
    ($buttonColor = get_theme_option('button_color')) || ($buttonColor = "#000000");
    ($buttonTextColor = get_theme_option('button_text_color')) || ($buttonTextColor = "#FFFFFF");
    ($titleColor = get_theme_option('header_title_color')) || ($titleColor = "#000000");
    ($useOriginalThumbnailSize = get_theme_option('use_original_thumbnail_size')) || ($useOriginalThumbnailSize = "0");
    ?>
	<!-- JavaScripts -->
	<?php
    queue_js_file('vendor/modernizr');
    queue_js_file('vendor/selectivizr', 'javascripts', array('conditional' => '(gte IE 6)&(lte IE 8)'));
    queue_js_file('vendor/respond');
    queue_js_file('vendor/jquery-accessibleMegaMenu');
    queue_js_file('globals');
    queue_js_file('default');
    queue_js_file('bootstrap/bootstrap.min');
	// Splide on homepage
    if ($bodyid == 'home') {
        queue_js_file('../lib/splide/js/splide.min');
    } 
	// Calls Matomo 
	queue_js_file('matomo');

    echo head_js();

    ?>

</head>

<?php echo body_tag(array('id' => @$bodyid, 'class' => @$bodyclass)); ?>
	<a href="#content" id="skipnav"><?php echo __('Skip to main content'); ?></a>
	<?php fire_plugin_hook('public_body', array('view'=>$this)); ?>

		<header role="banner" class="header">
			<?php if (get_theme_option('top_header_intro') !=''): ?>
			<div class="header__top">
				<div class="wrap">
				<p><?php echo get_theme_option('top_header_intro'); ?></p>

					<?php /*if (plugin_is_active('LocaleSwitcher')): ?>
                    <div class="header__languages">
                            <?php echo $this->localeSwitcher(); ?>
                    </div>
                    <?php endif;*/ ?>
					<?php if (plugin_is_active('SwitchLanguage')): ?>
					<div class="header__languages">
						<ul>
							<li><a href="?lang=fr">Fr</a></li>
							<li><a href="?lang=it">It</a></li>
						</ul>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="wrap">
				<div class="header__logo">
					<?php fire_plugin_hook('public_header', array('view'=>$this)); ?>
					<div id="site-title"><?php echo link_to_home_page(theme_logo()); ?></div>
				</div>
				<div class="header__connexion">
					<?php if ((get_theme_option('header_bloglink_label') !='') && (get_theme_option('header_bloglink_url') !='')): ?>
					<a href="<?php echo get_theme_option('header_bloglink_url'); ?>" target="_blank"><?php echo get_theme_option('header_bloglink_label'); ?></a>
					<?php endif; ?>

					<?php if (current_user()):?>
						<a href="/users/logout" title="<?php echo __('Log Out'); ?>" class="connect disconnect"><?php echo __('Log Out'); ?></a>
					<?php else: ?>
						<a href="/guest-user/user/login" title="<?php echo __('Log In'); ?>" class="connect"><?php echo __('Log In'); ?></a>
					<?php endif; ?>
				</div>
			</div>

			<div class="header__menu">
				<div class="wrap">
					<div class="menu-button button">Menu</div>
					<nav id="primary-nav" role="navigation">
						<?php echo public_nav_main(array('role' => 'navigation')); ?>
					</nav>
				</div>
			</div>

		</header>

		<div id="search-container" role="search">
			<div class="wrap wrap--xs">
				<?php if (get_theme_option('use_advanced_search')): ?>
				<?php echo search_form(array('show_advanced' => true)); ?>
				<?php else: ?>
				<?php echo search_form(); ?>
				<?php endif; ?>
			</div>
		</div>


		<div id="main-content" role="main" tabindex="-1">
			<?php fire_plugin_hook('public_content_top', array('view'=>$this)); ?>
