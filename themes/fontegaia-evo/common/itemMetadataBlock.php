<?php
function valuesCount($property)
{
    if (count(metadata('item', array('Dublin Core', $property), 'all')) > 1) {
        return true;
    } else {
        return false;
    }
}

$propertiesToDisplay = [  ["Creator", "Publisher", "Date", "Contributor"],
                          ["Subject", "Temporal", "Description", "Provenance"],
                          ["Type", "Format", "Language"],
                          ["Relation", "Identifier", "hasFormat"]
                        ];
?>

<?php foreach ($propertiesToDisplay as $propertyGroup): ?>
    <div class="item-contents__block">
      <?php foreach ($propertyGroup as $property): ?>
        <?php if (metadata('item', array('Dublin Core', $property))): ?>
          <div class="item__<?= $property ?>">
                <strong class="item__label"><?php echo __($property); echo __(':'); ?></strong>
                <?php if (valuesCount($property) == true): ?>
                      <ul>
                      <?php foreach (metadata('item', array('Dublin Core', $property), 'all') as $value):
                                echo "<li>" . __($value) . "</li>";
                            endforeach;?>
                      </ul>
                <?php else:
                      echo "<ul><li>";
                      echo __(metadata('item', array('Dublin Core', $property)));
                      echo "</li></ul>"; ?>
                <?php endif;?>
          </div>
        <?php endif; ?>
      <?php endforeach;?>
    </div>
<?php endforeach;?>
