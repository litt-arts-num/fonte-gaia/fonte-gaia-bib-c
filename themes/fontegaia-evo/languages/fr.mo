��    3      �  G   L      h     i     k     {     �     �  I   �     �     �            
         +     4     H     N     c     �     �     �     �  `   �  	     
        %     3     <     E     ^     j     �  	   �     �     �     �     �     �  3   �     "  	   4  }   >  =   �  	   �       "     R   =     �     �     �     �     �  `  �     -
     0
     C
     Z
     a
  _   
     �
     �
            
        *     3     M     T  #   l     �     �     �     �  l   �     6  	   >     H     \  
   m     x     �     �     �     �     �     �               6  A   ?     �     �  �   �  L   ?     �     �     �  a   �     6     G     g     v     z           !                  .           -   +      %   
                        "                   *   )          1             #                &      (   ,                                         /                     	      2   $          0   '   3    : Advanced search All the collections Author Can we contact you? Check this box if it is okay for us to contact you about this correction. Close Collection Items Collections Comments Contribute Creator: Current data for %s Date: Document unavailable Go to the other reading paths In this collection More My Contributions News Please describe the nature of this correction, or anything about it that we should know. Thanks! Publisher Publisher: Reading paths Register See also See the scanned document Show notice Simple search results Subjects Subjects: Submit Correction Submit your content Submit your correction Suggestions for item  Summary Thank you for taking the time to improve this site! View all %s items View item You can also leave general comments or suggestions in the "comments" section. An administrator will review your contribution. You can suggest corrections to the following fields for item  You must  You noticed an error? You want to propose a new content? before contributing. You can still leave your identity to site visitors anonymous. create an account documents in this collection log in of or Project-Id-Version: omeka-theme-fontegaia
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-17 15:41+0100
Last-Translator: Christophe Perrin <christophe@kaeness.fr>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.1
  : Recherche avancée Toutes les collections Auteur Pouvons-nous vous contacter ? Cochez cette case si vous êtes d'accord pour être contacté(e) à propos de cette correction. Fermer Documents de la  collection Les collections Commentaires Contribuer Auteur : Données actuelle pour %s Date : Document non disponible Voir les autres parcours de lecture Dans cette collection Voir Mes contributions Nouveautés Merci de nous expliquer la nature de votre correction ou toute autre chose que nous devrions savoir. Merci ! Editeur Editeur : Parcours de lecture Créer un compte Voir aussi Voir le document numérisé Voir la notice Résultats de recherche simple Sujets Sujets : Soumettre la correction Proposez votre document Proposer une correction Suggestions pour la notice  Sommaire Merci d'avoir pris le temps de nous aider à améliorer le site ! Voir tous les %s documents Voir le document Vous pouvez aussi laisser un commentaire ou une suggestion dans la section "commentaire". Un administrateur prendra votre contribution en compte. Vous pouvez suggérer des corrections pour les champs suivants de la notice  Vous devez  Vous remarquez une erreur ? Suggérer l'ajout d'un document pour pouvoir contribuer. Vous pourrez tout de même rester anonyme auprès des visiteurs du site. créer un compte documents dans cette collection vous connecter sur ou 