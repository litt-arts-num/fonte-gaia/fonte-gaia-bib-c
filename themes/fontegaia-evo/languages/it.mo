��    3      �  G   L      h     i     k     {     �     �     �     �  I   �          	       
     	   )     3     G     M     b     h     �     �     �     �  `   �       	   )  
   3     >     L     U     ^     w     �     �  	   �     �     �     �     �  3   �          "  }   2  =   �  	   �  R   �     K     ]     z     �     �  ^  �     �	     �	     �	     
     
     
     !
  R   7
     �
     �
     �
     �
     �
     �
     �
     �
     �
  "   �
          0     9     K  i   S     �     �     �     �     �  
   �  '   
     2     C     a  	   j     t     �     �     �  <   �  	   �     �  �     C   �     �  i   �     \     n     �     �     �        !   $                     *       )   -      '                                     "         ,   +         2             &                (   	   1   3                    .                     /              #                %   
       0            : Advanced search All the collections Année Author Author: Can we contact you? Check this box if it is okay for us to contact you about this correction. Close Collections Comments Contribute Créateur Current data for %s Date: Document unavailable Genre Go to the other reading paths In this collection More My Contributions News Please describe the nature of this correction, or anything about it that we should know. Thanks! Projet de recherche Publisher Publisher: Reading paths Register See also See the scanned document Show notice Simple search results Subjects Subjects: Submit Correction Suggestions for item  Sujet Summary Thank you for taking the time to improve this site! Thématique Type de support You can also leave general comments or suggestions in the "comments" section. An administrator will review your contribution. You can suggest corrections to the following fields for item  You must  before contributing. You can still leave your identity to site visitors anonymous. create an account documents in this collection log in of or Project-Id-Version: omeka-theme-fontegaia
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-17 15:46+0100
Last-Translator: Christophe Perrin <christophe@kaeness.fr>
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1
 : Ricerca avanzata Tutte le collezioni Anno Autore Autore: Possiamo contattarla? Selezionare questa casella se si desidera essere contattati per questa correzione. Chiudere Le collezioni Commenti Contribuire Autore Dati attuali per %s Data: Documento non disponibile Genere Vedi gli altri percorsi di lettura In questa collezione Scoprire I miei contributi Notizie Per favore, descrivete la natura di questa correzione o qualsiasi altra cosa che dovremmo sapere. Grazie! Progetto di ricerca Editore Editore: Percorsi di lettura Registro Vedi anche Visualizzare il documento digitalizzato Vedi  l'articolo Risultati di ricerca semplici Soggetti Soggetti: Invia la correzione Suggerimenti per l'articolo  Sogetto Sintesi Grazie per aver dedicato del tempo a migliorare questo sito! Argomento Tipo di ogetto materiale È anche possibile lasciare commenti o suggerimenti generali nella sezione "commenti". Un amministratore esaminerà il vostro contributo. È possibile suggerire correzioni ai seguenti campi per l'elemento  È necessario  prima di contribuire. È comunque possibile lasciare anonima la propria identità ai visitatori del sito. creare un account documenti di questa collezione accedere su o 