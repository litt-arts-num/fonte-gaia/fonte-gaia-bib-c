<?php
$pageTitle = __('Forgot Password');
echo head(array('title' => $pageTitle, 'bodyclass' => 'login'), $header);
?>

<div class="wrap wrap--lg breadcrumb">
	<p id="simple-pages-breadcrumbs"><?php echo link_to_home_page(__('Home')); ?> > <?php echo $pageTitle; ?></p>
</div>

<main class="wrap wrap--lg">
	<h1 class="page__title"><?php echo $pageTitle; ?></h1>

	<div class="wrap wrap--xs">
		<p id="login-links">
		<span id="backtologin"><?php echo link_to('users', 'login', __('Back to Log In')); ?></span>
		</p>

		<p class="clear"><?php echo __('Enter your email address to retrieve your password.'); ?></p>
		<?php echo flash(); ?>
		<form method="post" accept-charset="utf-8">
			<div class="field">        
				<label for="email"><?php echo __('Email'); ?></label>
				<?php echo $this->formText('email', @$_POST['email']); ?>
			</div>

			<input type="submit" class="submit" value="<?php echo __('Submit'); ?>" />
		</form>
	</div>
</main>
<?php echo foot(array(), $footer); ?>
