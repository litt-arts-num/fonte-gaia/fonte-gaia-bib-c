<?php
queue_js_file('login');
$pageTitle = __('Log In');
echo head(array('bodyclass' => 'login', 'title' => $pageTitle), $header);
?>

<div class="wrap wrap--lg breadcrumb">
	<p id="simple-pages-breadcrumbs"><?php echo link_to_home_page(__('Home')); ?> > <?php echo $pageTitle; ?></p>
</div>

<main class="wrap wrap--lg">
	<h1 class="page__title"><?php echo $pageTitle; ?></h1>

	<div class="wrap wrap--xs">
		<p id="login-links">
		<span id="backtosite"><?php echo link_to_home_page(__('Go to Home Page')); ?></span> |  <span id="register"><a href='/guest-user/user/register'><?php echo __('Register'); ?></a></span> | <span id="forgotpassword"><?php echo link_to('users', 'forgot-password', __('Lost your password?')); ?></span>
		</p>

		<?php echo flash(); ?>

		<?php echo $this->form->setAction($this->url('users/login')); ?>
	</div>
</main>

<?php echo foot(array(), $footer); ?>
