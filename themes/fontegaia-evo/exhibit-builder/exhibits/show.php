<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script> -->


<?php




$exhibit_class = '';
if (strstr(strtolower(metadata('exhibit', 'title')), 'parcours')) {
    $exhibit_class = 'parcours';
}

echo head(array(
    'title' => metadata('exhibit_page', 'title') . ' &middot; ' . metadata('exhibit', 'title'),
    'bodyclass' => 'exhibits show '.$exhibit_class));

    $exhibitNavOption = get_theme_option('exhibits_nav');

    // Si menu d'exposition en bas de page, desactive le lien sur la page en cours
    if ($exhibitNavOption != 'none'):
        echo '<style> .exhibits a[href$="'.current_url().'"] { pointer-events: none; opacity: 0.6; border: 0; box-shadow: none; } </style>';
    endif;
?>

<div class="wrap wrap--lg breadcrumb">
	<?php
        $exhibit_breadcrumb = str_replace('<br>', ' &gt; ', exhibit_builder_page_trail());

    ?>
	<p id="simple-pages-breadcrumbs"><?php echo link_to_home_page(__('Home')); ?> > <?php echo exhibit_builder_link_to_exhibit($exhibit); ?> > <?php echo $exhibit_breadcrumb; ?> </p>
</div>

<?php if ($exhibitNavOption == 'full'): ?>
<nav id="exhibit-pages" class="full">
	<?php echo exhibit_builder_page_nav(); ?>
</nav>
<?php endif; ?>

<div class="wrap <?php if ($exhibit_class == 'parcours'): echo 'wrap--lg'; else: echo 'wrap--md'; endif; ?>">
	<h1><span class="exhibit-page"><?php echo metadata('exhibit_page', 'title'); ?></span></h1>

	<?php if (count(exhibit_builder_child_pages()) > 0 && $exhibitNavOption == 'full'): ?>
	<nav id="exhibit-child-pages" class="secondary-nav">
		<?php echo exhibit_builder_child_page_nav(); ?>
	</nav>
	<?php endif; ?>

	<div role="main" id="exhibit-blocks">
	<?php exhibit_builder_render_exhibit_page(); ?>
	</div>

	<?php
    if ($exhibit_class == 'parcours'):
        echo '';
    else: ?>

	<div id="exhibit-page-navigation">
		<?php if ($prevLink = exhibit_builder_link_to_previous_page()): ?>
		<div id="exhibit-nav-prev">
		<?php echo $prevLink; ?>
		</div>
		<?php endif; ?>
		<?php if ($nextLink = exhibit_builder_link_to_next_page()): ?>
		<div id="exhibit-nav-next">
		<?php echo $nextLink; ?>
		</div>
		<?php endif; ?>
		<div id="exhibit-nav-up">
		<?php echo exhibit_builder_page_trail(); ?>
		</div>
	</div>
	<?php endif; ?>



	<nav id="exhibit-pages" class="exhibit-nav wrap wrap--md <?php echo $exhibit_nav_class; ?>">
		<p class="h2"><strong><?php if ($exhibit_class == 'parcours'): echo __('Go to the other reading paths'); else: echo __('Summary'); endif; ?></strong></p>
    <?php
      $exhibitCurrentPage = get_current_record('exhibit_page');

      if ($exhibit_class == 'parcours') {
          echo exhibit_builder_page_tree($exhibit, $exhibitCurrentPage);
      } else {
          /* REPLACES  echo exhibit_builder_page_tree($exhibit, $exhibit_page); */
          $pages = $exhibit->getPagesByParent();
          echo $this->partial('exhibits/pageTree.php', ['exhibit' =>$exhibit, 'pages' => $pages, 'currentPage' =>$exhibitCurrentPage]);
      }



    ?>

  <?php /* echo exhibit_builder_page_tree($exhibit, $exhibit_page); */
  // var_dump($exhibit->getPagesByParent());
  ?>

	</nav>
</div>


<?php echo foot(); ?>
