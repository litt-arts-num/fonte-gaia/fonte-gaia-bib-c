<?php

//
    /**
     * Return the tree of pages.
     */
     function exhibitPageTree($exhibit, $pages, $currentPage = null)
     {
         $pages = $exhibit->PagesByParent;
         if (!($pages && isset($pages[0]))) {
             return '';
         }

         $ancestorIds = _getAncestorIds($currentPage, $exhibit);

         $html = _renderListOpening();
         foreach ($pages[0] as $topPage) {
             $html .= _customRenderPageBranch($topPage, $currentPage, $ancestorIds, $pages, $exhibit);
         }
         $html .= '</div>';
         return $html;
     }


    /**
     * Recursively create the HTML for a "branch" (a page and its descendants)
     * of the tree.
     */
    function _renderPageBranch($page, $currentPage, $ancestorIds, $pages, $exhibit)
    {
        if ($currentPage && $page->id === $currentPage->id) {
            $html = '<li class="current">';
        } elseif ($ancestorIds && isset($ancestorIds[$page->id])) {
            $html = '<li class="parent">';
        } else {
            $html = '<li>';
        }

        $html .= '<a href="' . exhibit_builder_exhibit_uri($exhibit, $page) . '">'
              . metadata($page, 'menu_title') .'</a>';
        if (isset($pages[$page->id])) {
            $html .= '<ul>';
            foreach ($pages[$page->id] as $childPage) {
                $html .= _renderPageBranch($childPage, $currentPage, $ancestorIds, $pages, $exhibit);
            }
            $html .= '</ul>';
        }
        $html .= '</li>';
        return $html;
    }

    /**
     * Recursively create the HTML for a "branch" (a page and its descendants)
     * of the tree.
     */
    function _customRenderPageBranch($page, $currentPage, $ancestorIds, $pages, $exhibit)
    {
        if ($currentPage && $page->id === $currentPage->id) {
            $expanded = 'true';
            $divState = 'show';
            $buttonState = '';
        } elseif ($ancestorIds && isset($ancestorIds[$page->id])) {
            $expanded = 'true';
            $divState = 'show';
            $buttonState = '';
        } else {
            $expanded = 'false';
            $divState = '';
            $buttonState = 'collapsed';
        }
        $html = '<div class="accordion-item">
        <p class="accordion-header" id="heading-'. $page->id .'">'
        .'<button class="accordion-button '. $buttonState .'"
                    type="button" data-bs-toggle="collapse"
                    data-bs-target="#collapse-'. $page->id .'"
                    aria-expanded="'. $expanded .'"
                    aria-controls="collapse-'. $page->id .'">';
        $html .= metadata($page, 'menu_title') .'</button></p>';
        if (isset($pages[$page->id])) {
            $html .= '<div id="collapse-'. $page->id .'" class="accordion-collapse collapse ' . $divState .  '" aria-labelledby="heading-' . $page->id .'">
            <div class="accordion-body">
            <ul>';

            // Uncomment to access Expo Parent Page
            // <li><a href="' . exhibit_builder_exhibit_uri($exhibit, $page) . '" style="text-color:"> INTRODUCTION ' . /* metadata($page, 'menu_title') .*/ '</a></li>';

            // The original function is recursive here not quite because we call a different function (the original one) for every child page.
            foreach ($pages[$page->id] as $childPage) {
                $html .= _renderPageBranch($childPage, $currentPage, $ancestorIds, $pages, $exhibit);
            }
            $html .= '</div></div></ul>';
        }
        $html .= '</div>';
        return $html;
    }

    /**
     * Get the opening tag for the outermost list element.
     */
     function _renderListOpening()
     {
         return '<div class="accordion" id="accordionPanels">';
     }

    /**
     * Get the IDs of all pages that are ancestors of the current page.
     */
     function _getAncestorIds($currentPage, $exhibit)
     {
         $ancestorIds = array();
         if ($currentPage) {
             $pagesById = $exhibit->PagesById;
             $currentId = $currentPage->parent_id;
             while ($currentId) {
                 $currentPage = $pagesById[$currentId];
                 $ancestorIds[$currentPage->id] = $currentPage->id;
                 $currentId = $currentPage->parent_id;
             }
         }

         return $ancestorIds;
     }


echo exhibitPageTree($exhibit, $pages, $currentPage);
