<?php
$exhibit_class = '';
if (strstr(strtolower(metadata('exhibit', 'title')), 'parcours')) {
    $exhibit_class = 'parcours';
}
?>
<?php echo head(array('title' => metadata('exhibit', 'title'), 'bodyclass'=>'exhibits summary '.$exhibit_class)); ?>

<div class="wrap wrap--lg breadcrumb">
	<?php
        //$exhibit_breadcrumb = str_replace('<br>',' &gt; ' ,exhibit_builder_page_trail());

    ?>
	<p id="simple-pages-breadcrumbs"><?php echo link_to_home_page(__('Home')); ?> > <?php echo metadata('exhibit', 'title'); ?></p>
</div>

<div class="wrap  <?php if ($exhibit_class == 'parcours'): echo 'wrap--lg'; else: echo 'wrap--md'; endif; ?>">
	<h1><?php echo metadata('exhibit', 'title'); ?></h1>
	<?php echo exhibit_builder_page_nav(); ?>

	<div id="primary">
	<?php if ($exhibitDescription = metadata('exhibit', 'description', array('no_escape' => true))): ?>
	<div class="exhibit-description">
		<?php echo $exhibitDescription; ?>
	</div>
	<?php endif; ?>

	<?php if (($exhibitCredits = metadata('exhibit', 'credits'))): ?>
	<div class="exhibit-credits">
		<h3><?php echo __('Credits'); ?></h3>
		<p><?php echo $exhibitCredits; ?></p>
	</div>
	<?php endif; ?>
	</div>

	<?php
    if ($exhibit_class == 'parcours'):

    $pageTree = exhibit_builder_page_tree();
            if ($pageTree):
            ?>
							<nav id="exhibit-pages" class="exhibit-nav wrap wrap--xs">
								<p class="h2"><strong><?php echo __('Go to the other reading paths');?></strong></p>
								<?php echo $pageTree; ?>
							</nav>
			<?php endif;
    else:?>
							<nav id="exhibit-pages" class="exhibit-nav wrap wrap--md">
								<p class="h2"><strong><?php echo __('Summary');?></strong></p>
							<?php
              $pages = $exhibit->getPagesByParent();
              echo $this->partial('exhibits/pageTree.php', ['exhibit' =>$exhibit, 'pages' => $pages, 'currentPage' =>null]);
                            ?>
							</nav>
		<?php endif;?>
</div>

<?php echo foot(); ?>
