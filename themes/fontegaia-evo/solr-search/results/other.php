<!-- Header. -->
<div class="result__contents">
  <div class="result__img" style="max-height:120px;">
  </div>

  <div class="result__content">
    <!-- Record URL. -->
    <?php $url = SolrSearch_Helpers_View::getDocumentUrl($doc); ?>

    <div class="result__content--limited">
      <!-- Title. -->
      <h2 class="result__title">
        <a href="<?php echo $url; ?>">
        <?php
                      $title = is_array($doc->title) ? $doc->title[0] : $doc->title;
                      if (empty($title)) {
                          $title = '<i>' . __('Untitled') . '</i>';
                      }
                      echo "Page : " . $title;
                  ?>
        </a>
      </h2>
      <div class="result__datas">
        <div>
        </div>
      </div>
    </div>

    <!-- Highlighting. -->
    <?php if (get_option('solr_search_hl') && array_key_exists('q', $_GET) && ($_GET['q']!='')): ?>
      <?php $tabHighRandomId = bin2hex(random_bytes(4)); ?>
      <?php
              $snippetHTML = '';
              $nbOccurence = 0;
              foreach ($results->highlighting->{$doc->id} as $field):
                  foreach ($field as $hl):
                      $nbOccurence++;
                      $snippetHTML .= '<li class="snippet">'.strip_tags($hl, '<em>').'</li>';
                  endforeach;
              endforeach;
              ?>
      <?php if ($nbOccurence > 0): ?>
        <div class="result__highlighting block-accordion">
          <input id="tab-hl-<?= $tabHighRandomId ?>" type="checkbox">
          <label for="tab-hl-<?= $tabHighRandomId ?>" class="accordion-label"><span><?php echo array_key_exists('q', $_GET) ? $_GET['q'] : ''; ?> : <?php echo $nbOccurence ?> </span>occurrence<?php echo ($nbOccurence > 1) ? 's' :'';?></label>
          <ul class="accordion-content">
            <?php echo $snippetHTML; ?>
          </ul>
        </div>
      <?php endif; ?>
    <?php endif; ?>
  </div>
</div>
