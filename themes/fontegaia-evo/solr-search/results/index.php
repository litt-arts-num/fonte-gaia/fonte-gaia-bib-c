<?php

/**
 * @package     omeka
 * @subpackage  solr-search
 * @copyright   2012 Rector and Board of Visitors, University of Virginia
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 */

?>
<!-- Bootstrap : Accordion for the facets
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->


<?php queue_css_file('results'); ?>
<?php echo head(array('title' => __('Solr Search'),'bodyclass' => 'search-results'));?>

<div class="search-results__header">
	<div class="wrap">
		<h1 class="page__title"><?php echo __('Search') ?></h1>
	</div>
</div>

<!-- Search form. -->
<div class="solr search-container">
	<div class="wrap wrap--xs">
		<form id="solr-search-form">
			<input type="text" title="<?php echo __('Search keywords') ?>" name="q" value="<?php
                    echo array_key_exists('q', $_GET) ? $_GET['q'] : '';
                ?>" />
			<!-- <input type="submit" value="Recherchez" /> -->
			<?php echo $this->formButton('submit_search', $options['submit_value'], array('type' => 'submit')); ?>
		</form>
	</div>
</div>

<div class="search-results__container wrap">

	<div class="search-results__facets">
			<h2><?php echo __('Limit your search'); ?></h2>
		<!-- Applied facets. -->
		<div class="search-results__facets applied">
			<ul>
				<!-- Get the applied facets. -->
				<?php foreach (SolrSearch_Helpers_Facet::parseFacets() as $f): ?>
					<li>
						<!-- Facet label. -->
						<?php $label = SolrSearch_Helpers_Facet::keyToLabel($f[0]); ?>
						<span class="applied-facet__label"><?php echo __($label); ?></span> >
						<span class="applied-facet__value"><?php echo $f[1]; ?></span>

						<!-- Remove link. -->
						<?php $url = SolrSearch_Helpers_Facet::removeFacet($f[0], $f[1]); ?>
						<span class="applied-facet__remove"> [&nbsp;<a href="<?php echo $url; ?>"><?php echo __('Remove'); ?></a>&nbsp;]</span>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>

		<!-- Facets. -->
		<div class="search-results__facets">

			<div class="accordions">
				<?php foreach ($results->facet_counts->facet_fields as $name => $facets): ?>

					<!-- Does the facet have any hits? -->
					<?php if (count(get_object_vars($facets))): ?>
						<?php
                            $label = SolrSearch_Helpers_Facet::keyToLabel($name);
                            $tabRandomId = bin2hex(random_bytes(4));
                        ?>
						<div class="block-accordion">
							<input id="tab-<?= $tabRandomId ?>" type="checkbox">
							<label for="tab-<?= $tabRandomId ?>" class="accordion-label"><span><?php echo __($label); ?></span></label>

							<div class="accordion-content">
								<ul>
									<!-- Facets. -->
									<?php foreach ($facets as $value => $count): ?>
										<li class="<?php echo $value; ?>">
											<!-- Facet URL. -->
											<?php $url = SolrSearch_Helpers_Facet::addFacet($name, $value); ?>
											<!-- Facet link. -->
											<a href="<?php echo $url; ?>" class="facet-value">
												<?php echo $value; ?>
											</a>
											<!-- Facet count. -->
											<span class="facet-count"><?php echo $count; ?></span>
										</li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div> <!-- end of accordeon item -->
					<?php endif; ?> <!--if facet has any hits -->

				<?php endforeach; ?>
			</div>
		</div> <!-- facets' end -->
	</div>



	<!-- Results. -->
	<div class="results">
		<?php $numFound = $results->response->numFound; ?>
		<?php if ($numFound > 0): ?>
			<!-- Number found. -->

			<div class="results-list__header">

				<div class="results-list__number"><span><?php echo $results->response->numFound; ?></span> resultats</div>
				<?php
                echo $this->partial('results/sort.php', array(
                    'query' => $query,
                    'sortOptions' => $sortOptions,
                ));
                ?>

				<div class="results-list__pagination">
					<?php echo pagination_links(); ?>
				</div>
			</div>
			<div class="results-list">
				<?php foreach ($results->response->docs as $doc): ?>
					<?php $record = get_db()->getTable($doc->model)->find($doc->modelid); ?>

					<!-- Document. -->
					<div class="result">

						<?php

                        if (get_class($record) == "Item") {
                            echo $this->partial('/results/item.php', ['results' =>$results, 'doc' => $doc, 'record' => $record]);
                        } else {
                            echo $this->partial('/results/other.php', ['results' =>$results, 'doc' => $doc, 'record' => $record]);
                        }
                        ?>

					</div>

				<?php endforeach; ?>
			</div>

			<?php else: ?>
					<?php $no_results_text = get_option('solr_search_no_results_text'); ?>
					<?php if (!empty($no_results_text)): ?>
							<?php echo $no_results_text; ?>
					<?php else: ?>
							<?php echo __('No results found'); ?>
					<?php endif; ?>
			<?php endif; ?>

			<div id="solr_pagination_bottom">
				<?php echo pagination_links(); ?>
			</div>

	</div>

</div><!-- .wrap -->

<?php echo foot();
