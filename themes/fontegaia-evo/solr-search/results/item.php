<!-- Header. -->
<div class="result__contents">
  <div class="result__img">
    <?php if (metadata($record, 'has files')): ?>

      <?php //echo link_to_item(item_image(null, array('alt' => $itemTitle)));?>
      <?php echo link_to($record, 'show', item_image(null, array(), 0, $record)); ?>

    <?php
              elseif ($collectionSubstitute = get_theme_option('item_default_img')):

              $collectionSubstitute = '<img src="/files/theme_uploads/'.$collectionSubstitute.'" alt="">';

              echo $collectionSubstitute;
          ?>
    <?php endif; ?>

  </div>

  <div class="result__content">
    <!-- Record URL. -->
    <?php $url = SolrSearch_Helpers_View::getDocumentUrl($doc); ?>

    <div class="result__content--limited">
      <!-- Title. -->
      <h2 class="result__title">
        <a href="<?php echo $url; ?>">
        <?php
                      $title = is_array($doc->title) ? $doc->title[0] : $doc->title;
                      if (empty($title)) {
                          $title = '<i>' . __('Untitled') . '</i>';
                      }
                      echo $title;
                  ?>
        </a>
      </h2>
      <div class="result__datas">
        <div>
          <?php
                          $creator = metadata($record, array('Dublin Core', 'Creator'));
                          $publisher = metadata($record, array('Dublin Core', 'Publisher'));
                          $date = metadata($record, array('Dublin Core', 'Date'));
                      ?>
          <?php if ($creator): ?>
            <p class="author"><?php echo '<span class="label">'.__('Author') . ' : ' .'</span> '.$creator; ?> </p>
          <?php endif; ?>

          <?php if ($publisher): ?>
            <p class="publisher"><?php echo '<span class="label">'.__('Publisher') . ' : ' .'</span> '.$publisher; ?></p>
          <?php endif; ?>

          <?php if ($date): ?>
            <p class="date"><?php echo '<span class="label">'.__('Date') . ' : '.'</span> '.$date; ?></p>
          <?php endif; ?>
        </div>


        <?php $subjects = metadata($record, array('Dublin Core', 'Subject'), array('all' => true)); ?>
        <?php if (!empty($subjects)): ?>
          <p class="subjects">
            <?php echo '<span class="label">'.__('Subjects:').'</span>'; ?>
            <span><?php echo implode(' | ', $subjects); ?></span>
          </p>
        <?php endif; ?>
      </div>
    </div>



    <!-- Highlighting. -->
    <?php if (get_option('solr_search_hl') && array_key_exists('q', $_GET) && ($_GET['q']!='')): ?>
      <?php $tabHighRandomId = bin2hex(random_bytes(4)); ?>
      <?php
              $snippetHTML = '';
              $nbOccurence = 0;
              foreach ($results->highlighting->{$doc->id} as $field):
                  foreach ($field as $hl):
                      $nbOccurence++;
                      $snippetHTML .= '<li class="snippet">'.strip_tags($hl, '<em>').'</li>';
                  endforeach;
              endforeach;
              ?>
      <?php if ($nbOccurence > 0): ?>
        <div class="result__highlighting block-accordion">
          <input id="tab-hl-<?= $tabHighRandomId ?>" type="checkbox">
          <label for="tab-hl-<?= $tabHighRandomId ?>" class="accordion-label"><span><?php echo array_key_exists('q', $_GET) ? $_GET['q'] : ''; ?> : <?php echo $nbOccurence ?> </span>occurrence<?php echo ($nbOccurence > 1) ? 's' :'';?></label>
          <ul class="accordion-content">
            <?php echo $snippetHTML; ?>
          </ul>
        </div>
      <?php endif; ?>
    <?php endif; ?>
  </div>
</div>
