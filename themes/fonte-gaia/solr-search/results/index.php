<?php

/**
 * @package     omeka
 * @subpackage  solr-search
 * @copyright   2012 Rector and Board of Visitors, University of Virginia
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 */

?>
<!-- Bootstrap : Accordion for the facets -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<?php queue_css_file('results'); ?>
<?php echo head(array('title' => __('Solr Search')));?>

<div id="solr_wrapper">
    <!-- Search form. -->
      <div class="solr">
        <form id="solr-search-form" >
          <span class="float-wrap">
            <input type="text" title="<?php echo __('Search keywords') ?>" name="q" value="<?php
              echo array_key_exists('q', $_GET) ? $_GET['q'] : '';
            ?>" />
          </span>
          <input type="submit" value="Recherchez" />
        </form>
      </div>

  <!-- Applied facets. -->
  <div id="solr-applied-facets">
    <ul>
      <!-- Get the applied facets. -->
      <?php foreach (SolrSearch_Helpers_Facet::parseFacets() as $f): ?>
        <li>
          <!-- Facet label. -->
          <?php $label = SolrSearch_Helpers_Facet::keyToLabel($f[0]); ?>
          <span class="applied-facet-label"><?php echo $label; ?></span> >
          <span class="applied-facet-value"><?php echo $f[1]; ?></span>

          <!-- Remove link. -->
          <?php $url = SolrSearch_Helpers_Facet::removeFacet($f[0], $f[1]); ?>
          (<a href="<?php echo $url; ?>">remove</a>)
        </li>
      <?php endforeach; ?>
    </ul>
  </div>


  <!-- Facets. -->
  <div id="solr-facets">
    <h2><?php echo __('Limit your search'); ?></h2>

    <div class="accordion" id="accordionPanelsStayOpenExample"> <!-- BOOTSTRAP -->
    <?php foreach ($results->facet_counts->facet_fields as $name => $facets): ?>

      <!-- Does the facet have any hits? -->
      <?php if (count(get_object_vars($facets))): ?>
        <?php $label = SolrSearch_Helpers_Facet::keyToLabel($name);
              // accordid added with bootstrap (Theo)
              $accordid = text_to_id($label);
        ?>

        <div class="accordion-item">
            <!-- Facet label. -->
            <h2 class="accordion-header" id="<?php echo('panelsStayOpen-heading' . $accordid);  ?>" >
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"  data-bs-target="<?php echo('#panelsStayOpen-collapse' . $accordid);  ?>" aria-expanded="false" aria-controls="<?php echo('panelsStayOpen-collapse' . $accordid);  ?>">
                <strong><?php echo $label; ?></strong>
              </button>
            </h2>

            <div id="<?php echo('panelsStayOpen-collapse' . $accordid);  ?>" class="accordion-collapse collapse" aria-labelledby="<?php echo('panelsStayOpen-heading' . $accordid);  ?>">
              <div class="accordion-body">
                <ul>
                  <!-- Facets. -->
                  <?php foreach ($facets as $value => $count): ?>
                    <li class="<?php echo $value; ?>">
                      <!-- Facet URL. -->
                      <?php $url = SolrSearch_Helpers_Facet::addFacet($name, $value); ?>
                      <!-- Facet link. -->
                      <a href="<?php echo $url; ?>" class="facet-value">
                        <?php echo $value; ?>
                      </a>
                      <!-- Facet count. -->
                      (<span class="facet-count"><?php echo $count; ?></span>)
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
            </div>
          </div> <!-- end of accordeon item -->
        <?php endif; ?> <!--if facet has any hits -->

      <?php endforeach; ?>
    </div>
  </div> <!-- facets' end -->


  <!-- Results. -->
  <div id="solr-results">
      <?php $numFound = $results->response->numFound; ?>
      <?php if ($numFound > 0): ?>
        <!-- Number found. -->
      <div id="solr_results_head">
        <h2 id="num-found">
          <?php echo $results->response->numFound; ?> resultats
        </h2>

        <div id="solr_pagination_top">
          <?php echo pagination_links(); ?>
        </div>
      </div>
        <?php
        echo $this->partial('results/sort.php', array(
          'query' => $query,
          'sortOptions' => $sortOptions,
        ));
        ?>

        <?php foreach ($results->response->docs as $doc): ?>


          <!-- Document. -->
          <div class="result">

            <!-- Header. -->
            <div class="result-header">

              <!-- Record URL. -->
              <?php $url = SolrSearch_Helpers_View::getDocumentUrl($doc); ?>

              <!-- Title. -->
              <a href="<?php echo $url; ?>" class="result-title"><?php
                      $title = is_array($doc->title) ? $doc->title[0] : $doc->title;
                      if (empty($title)) {
                          $title = '<i>' . __('Untitled') . '</i>';
                      }
                      echo $title;
                  ?></a>

            </div>

            <!-- Highlighting. -->
            <?php if (get_option('solr_search_hl')): ?>
              <ul class="hl">
                <?php foreach ($results->highlighting->{$doc->id} as $field): ?>
                  <?php foreach ($field as $hl): ?>
                    <li class="snippet"><?php echo strip_tags($hl, '<em>'); ?></li>
                  <?php endforeach; ?>
                <?php endforeach; ?>
              </ul>
            <?php endif; ?>

            <?php
              $item = get_db()->getTable($doc->model)->find($doc->modelid);
              echo item_image_gallery(
                  array('wrapper' => array('class' => 'gallery')),
                  'square_thumbnail',
                  false,
                  $item
              );
            ?>

          </div>

        <?php endforeach; ?>
      <?php else: ?>
          <?php $no_results_text = get_option('solr_search_no_results_text'); ?>
          <?php if (!empty($no_results_text)): ?>
              <?php echo $no_results_text; ?>
          <?php else: ?>
              <?php echo __('No results found'); ?>
          <?php endif; ?>
      <?php endif; ?>

      <div id="solr_pagination_bottom">
        <?php echo pagination_links(); ?>
      </div>

  </div>

</div>


<?php echo foot();
