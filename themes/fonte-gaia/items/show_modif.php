<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


<?php echo head(array('title' => metadata('item', array('Dublin Core', 'Title')),'bodyclass' => 'items show')); ?>
    <?php /*echo all_element_texts('item'); */?>

 <?php /*   SN
    <?php if ((get_theme_option('Item FileGallery') == 0) && metadata('item', 'has files')): ?>
    <?php echo files_for_item(array('imageSize' => 'fullsize')); ?>
    <?php endif; ?>
*/ ?>

<!-- Bootstrap : Accordion for the facets -->


<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Description</button>
    <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Fac-Similé</button>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
    <div id="primary">
        <!-- The following returns all of the files associated with an item. -->
        <?php if ((get_theme_option('Item FileGallery') == 1) && metadata('item', 'has files')): ?>
        <div id="itemfiles" class="element">
            <h2><?php echo __('Files'); ?></h2>
            <?php echo item_image_gallery(); ?>
        </div>
        <?php endif; ?>

        <!-- The following prints a list of all tags associated with the item -->
        <?php if (metadata('item', 'has tags')): ?>
        <div id="item-tags" class="element">
            <h2><?php echo __('Tags'); ?></h2>
            <div class="element-text"><?php echo tag_string('item'); ?></div>
        </div>
        <?php endif;?>

        <?php if ($description = metadata('item', array('Dublin Core', 'Title'), array('snippet'=>250))): ?>
        <div class="item-description">
            <h2 id="main-title"><?php echo $description; ?></h2>
        </div>
        <?php endif; ?>

        <?php if (get_theme_option('Item Filter Metadata') == 1): ?>

        <?php echo all_element_texts('item'); ?>
        <div id='more-meta'><button class='short'><?php echo __('Voir plus'); ?></button><button class='long'><?php echo __('Voir moins'); ?></button></div>
          <!-- If the item belongs to a collection, the following creates a link to that collection. -->
          <?php /*if (metadata('item', 'Collection Name')): */?>
          <!--<div class="item-description">
            <b><?php /*echo __('Collection'); */?></b> : <?php /*echo link_to_collection_for_item(); */?>
          </div>-->
          <?php /*endif; */?>
        <?php endif; ?>

        <!-- The following prints a citation for this item. -->
    <!--
        <div class="item-description">
           <b><?php echo __('Citation'); ?></b> : <?php echo metadata('item', 'citation', array('no_escape' => true)); ?>
        </div>
    -->
    </div><!-- end primary -->


    <aside id="sidebar">
      <div id="contribution">
        <a href="<?php echo WEB_ROOT ?>/contribution">Soumettre un contenu</a>
      </div>
      <?php $corrections = get_specific_plugin_hook_output('Corrections', 'public_items_show', array('view' => $view, 'item' => $item));
        if ($corrections) : ?>
          <div id="corrections">
            <?php echo $corrections; ?>
            </div>
    <?php endif; ?>

    <?php if (metadata('item', array('Dublin Core', 'Has Version'))) : ?>
      <div id="digital-edition">
        <div class="digital-edition">
           <h2><a  href="<?php echo metadata('item', array('Dublin Core', 'Has Version')); ?>">Consulter l'édition numérique</a></h2>
         </div>
      </div>
     <?php endif; ?>

    <!--
        <div class="item-img">
            <?php echo item_image('thumbnail'); ?>
        </div>
    -->
    </aside>

  </div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
    <div class="item hentry visionneuse">
      <?php
        if (plugin_is_active('UniversalViewer')) {
            echo get_specific_plugin_hook_output('UniversalViewer', 'public_items_show', array('view' => $view, 'item' => $item));
        } else {
            if ((get_theme_option('Item FileDisplay') == 0) && metadata('item', 'has files')) {
                echo '  <div id="item-images">';
                echo files_for_item();
                echo "</div>";
            }
        }
      ?>
    </div>


<!-- Displays a link to the first PDF file attached to the current Item.  -->
    <?php
      $CurrentItemFiles = $item->Files;
      foreach ($CurrentItemFiles as $file) {
          if ($file["mime_type"] == "application/pdf") {
              $pdfFile = ($file["original_filename"]);
              break;
          }
      }
      ?>
    <?php if ($pdfFile): ?>
      <div id="link-section">
        <div id="link-pdf">
           <h2>
             <a href="<?php echo $pdfFile; ?> " target="_blank" rel="noopener noreferrer" >Accès au pdf</a>
           </h2>
        </div>
      </div>
    <?php endif; ?>

  </div>
</div>




<div id="comments">
   <?php echo get_specific_plugin_hook_output('Commenting', 'public_items_show', array('view' => $view, 'item' => $item)); ?>
</div>

<ul class="item-pagination navigation">
    <li id="previous-item" class="previous"><?php echo link_to_previous_item_show(); ?></li>
    <li id="next-item" class="next"><?php echo link_to_next_item_show(); ?></li>
</ul>

<?php echo foot(); ?>
