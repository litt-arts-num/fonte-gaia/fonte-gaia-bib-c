</div><!-- end content -->

<footer role="contentinfo">


     <!--   <div id="custom-footer-text">
                        <p><a href="/contact">Contact</a> - <a href="/a-propos">A Propos</a> - <a href="/mentions-legales">Mentions légales</a>
</p><div style="text-align:center;">

<p><a href="https://www.univ-grenoble-alpes.fr/"><img alt="Université Grenoble Alpes" src="https://www.univ-grenoble-alpes.fr/uas/SITEUI/LOGO/logo+bleu.svg" width="112" height="70"></a><a href="http://www.msh-alpes.fr/"><img alt="Maison des Sciences de l'homme" src="https://www.msh-alpes.fr/sites/msh-alpes/files/logo.png" width="180" height="81"></a><a href="https://www.unipd.it/en/"><img alt="Università degli Studi di Padova" src="https://www.unipd.it/sites/unipd.it/themes/unipd_2017/img/loghi800/800-white.png" width="202" height="55"></a><a href="https://www.uniroma1.it/it/pagina-strutturale/home"><img alt="Sapienza - Università di Roma" src="https://www.uniroma1.it/sites/default/files/images/logo/sapienza-big.png" width="165" height="80"></a><a href="http://www.univ-paris3.fr/"><img alt="Sorbonne" src="https://www.dbu.univ-paris3.fr/images/service-commun-de-la-documentation/header/univ-paris3-logo.jpg" width="116" height="84"></a><a href="https://www.unibo.it/it"><img alt="Università di Bologna" src="https://www.unibo.it/it/++theme++unibotheme.portale/img/header/sigillo1x_xl_zaky.png" width="242" height="50"></a> <a href="https://www.uniud.it/"><img alt="Università degli Studi di Udine" src="https://www.uniud.it/++theme++uniudine-theme/static/images/logo_small.png" width="169" height="47"></a> <a href="https://www.univ-tours.fr/site-de-l-universite/accueil--706278.kjsp"><img alt="Université de Tours" src="https://upload.wikimedia.org/wikipedia/fr/thumb/d/d0/UnivTours-logo.png/150px-UnivTours-logo.png" width="166" height="43"></a><a href="https://www.collexpersee.eu"><img alt="CollEx Persée" src="https://www.collexpersee.eu/wp-content/themes/collex/img/logo_collex_persee_texte.svg" width="169" height="70"></a><a href="https://litt-arts.univ-grenoble-alpes.fr/"><img alt="CollEx Persée" src="https://litt-arts.univ-grenoble-alpes.fr/images/littearts/logo.png" width="92" height="98"></a></p>
</div><p></p>
                                </div>-->




        <div id="custom-footer-text">
            <?php if ( $footerText = get_theme_option('Footer Text') ): ?>
            <p><?php echo $footerText; ?></p>
            <?php endif; ?>
            <?php if ((get_theme_option('Display Footer Copyright') == 1) && $copyright = option('copyright')): ?>
                <p><?php echo $copyright; ?></p>
            <?php endif; ?>
        </div>


    <?php fire_plugin_hook('public_footer', array('view' => $this)); ?>
</footer>

</div><!--end wrap-->

<script type="text/javascript">
jQuery(document).ready(function () {
    Omeka.showAdvancedForm();
    Omeka.skipNav();
    Omeka.megaMenu("#top-nav");
    Seasons.mobileSelectNav();
});
</script>

</body>

</html>
