<?php echo head(array('bodyclass' => 'contact-form')); ?>
<div id="primary">
  <div id="contact-form">
  <h1 style="margin-left:2em;"><?php echo html_escape(get_option('simple_contact_form_thankyou_page_title')); // Not HTML ?></h1>
  <p><?php echo get_option('simple_contact_form_thankyou_page_message'); ?> </p>
  </div>
</div>
<?php echo foot(); ?>