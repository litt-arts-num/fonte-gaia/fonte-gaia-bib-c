// Adds nice little ::after arrows to the menu div having sub items

jQuery(function($) {
  $(document).ready(function(){
    var ul_menu = $( '#top-nav > ul > li > .sub-nav > li:has(ul)' );
    //console.log(ul_menu.length);

    if (ul_menu.length > 0){
          //console.log("détecté " + ul_menu.length);
          (ul_menu.children('a')).addClass( 'sub-menu-arrow' );
          //ul_menu.addClass( 'sub-menu-arrow' );
                            }
                            });
                    }
      );

// Computes the value of the top: property of sub sub menus, so that they are vertically centered at the level of their parent's middle.

/*
jQuery(function($) {
  $(window).load(function(){
    var sub_menu = $ ('#top-nav > ul > li > .sub-nav > li:has(ul)');
    sub_menu.each(function() {
      console.log($(this));
      var parent_height = $(this).height();
      var children_height = $(this).children("ul").height();
      var parent_middle = ((parent_height - children_height)/2);
      console.log(parent_height);
      console.log(parent_middle);
      $(this).children("ul").css({'top' : parent_middle  });

                              });
                            });
                    }
      );
*/
