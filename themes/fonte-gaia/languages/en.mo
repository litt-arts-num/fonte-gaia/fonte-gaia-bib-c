��    >        S   �      H  
   I     T  
   `     k     ~     �     �     �     �  
   �     �     �     �  
   �     �               1     7     =     X     ]     w     |     �     �     �     �     �  	   �     �     
          #     *     ?     N     ]     l     y     �     �  1   �     �     �     �  	                  "  4   )  $   ^     �     �     �     �     �     �     �  
   �     �  N  	  
   W
     b
  
   n
     y
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
  
   �
               )     ?     E     K     f     k     �     �     �     �     �     �     �  	   �               $     1     8     M     \     k     z     �     �     �  1   �     �     �       	        #     +     0  4   7  $   l     �     �     �     �     �     �     �  
   �     	         .             /       6   ,   5   =   (          "                              '      !      >                    1   $      +   -   #   &             4   8   <   7   :          	                 9       ;   0           
                            )               %   2          *   3           (%s total) Add a Field Browse All Browse Collections Browse Exhibits Browse Exhibits by Tag Browse Items Browse by Tag Citation Collection Contributors Creator Date Date Added Featured Collection Featured Item Featured/Non-Featured Files First Items in the %s Collection Last Narrow by Specific Fields Next Only Featured Items Only Non-Featured Items Only Non-Public Items Only Public Items Previous Public/Non-Public Publisher Recently Added Items Record Type Remove field Search Search By Collection Search By Tags Search By Type Search By User Search Field Search Items Search Terms Search Type Search by a range of ID#s (example: 1-4, 156, 79) Search for Keywords Search for items Skip to main content Sort by:  Subject Tags Tags:  There are currently no items within this collection. There are no exhibits available yet. Title View All Items View the items in %s Your query returned no results. contains does not contain is empty is exactly is not empty Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-11 16:20+0200
Last-Translator: Camille <camille.desiles@univ-grenoble-alpes.fr>
Language-Team: English
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=ASCII
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 (%s total) Add a Field Browse All Browse Collections Browse Exhibits Browse Exhibits by Tag Browse Items Browse by Tag Citation Collection Contributors Creator Date Date Added Featured Collection Featured Item Featured/Non-Featured Files First Items in the %s Collection Last Narrow by Specific Fields Next Only Featured Items Only Non-Featured Items Only Non-Public Items Only Public Items Previous Public/Non-Public Publisher Recently Added Items Record Type Remove field Search Search By Collection Search By Tags Search By Type Search By User Search Field Search Items Search Terms Search Type Search by a range of ID#s (example: 1-4, 156, 79) Search for Keywords Search for items Skip to main content Sort by:  Subject Tags Tags:  There are currently no items within this collection. There are no exhibits available yet. Title View All Items View the items in %s Your query returned no results. contains does not contain is empty is exactly is not empty 